<?php

/**
 * Plugin Name: O Lekach
 * Description: Zestaw rozszerzeń serwisu olekach.pl 
 * Author: Kamil Ryczek <kamil@piszekod.pl>
 * Author URI: https://devjam.pl
 * Version: 1.0
 *  
 * @author Kamil Ryczek <kamilk@piszekod.pl>
 * @copyright (c) 2019, O Lekach
 * @package olekach.pl
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

setlocale(LC_ALL, "pl_PL");
date_default_timezone_set('Europe/Warsaw');

define( 'PHARMA_CLI_VERSION', '1.0' );
define( 'PHARMA_CLI_DIR', __DIR__ );
define( 'PHARMA_CLI_SETTINGS_NAME', 'pharma_cli_settings' );
define( 'PHARMACY_DB_TABLE', 'pharmacy' );
define( 'PHARMACY_OPENINGS_DB_TABLE', 'pharmacy_openings' );

require_once PHARMA_CLI_DIR . '/entity/class-pharmacy.php';
require_once PHARMA_CLI_DIR . '/entity/class-pharmacy_openings.php';
require_once PHARMA_CLI_DIR . '/includes/functions.php';
require_once PHARMA_CLI_DIR . '/includes/class-spintax.php';
require_once PHARMA_CLI_DIR . '/includes/class-pharma.php';
require_once PHARMA_CLI_DIR . '/includes/class-pharma-post-types.php';
require_once PHARMA_CLI_DIR . '/includes/class-pharma-taxonomies.php';
require_once PHARMA_CLI_DIR . '/includes/class-pharma-cli-settings-page.php';
require_once PHARMA_CLI_DIR . '/includes/class-pharma-cli-db.php';
require_once PHARMA_CLI_DIR . '/includes/class-wp-bootstrap-navwalker.php';
require_once PHARMA_CLI_DIR . '/widgets/class-pharma-post-read-more-widget.php';
require_once PHARMA_CLI_DIR . '/widgets/class-pharma-nearest-pharmacies-widget.php';
require_once PHARMA_CLI_DIR . '/widgets/class-pharma-pharmacies-more-widget.php';
require_once PHARMA_CLI_DIR . '/widgets/class-pharma-cities-in-state-widget.php';
require_once PHARMA_CLI_DIR . '/widgets/class-pharma-cities-pharmacies-map-widget.php';
require_once PHARMA_CLI_DIR . '/widgets/class-pharma-synonymous-text-widget.php';

if ( defined( 'WP_CLI' ) && WP_CLI ) {
	require_once PHARMA_CLI_DIR . '/commands/class-import-pharmacy-data-cli.php';
}

register_activation_hook( __FILE__,		array( 'Pharma_CLI_DB', 'activation' ) );
register_deactivation_hook( __FILE__,	array( 'Pharma_CLI_DB', 'deactivation' ) );

\Pharma::init();