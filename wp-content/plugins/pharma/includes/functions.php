<?php

/**
 * @author Kamil Ryczek <kamil.ryczek@artefakt.pl>
 * @copyright (c) 2019, Artefakt
 * @package olekach.pl
 */

if( function_exists('acf_add_options_page') ) {	
    acf_add_options_page();
}

/**
 * Find nearest pharmacies
 * @see https://developers.google.com/maps/solutions/store-locator/clothing-store-locator#findnearsql
 * 
 * @global wpdb() $wpdb
 * @param float $lat
 * @param float $lon
 * @param integer $distance
 * @param integer $limit
 * @return array
 */
function get_nearest_pharmacies_by_lat_lon($lat, $lon, $exclude = array(), $distance = 1, $limit = 20) {
	global $wpdb;
	$where = array('1=1');
	if ( !empty($exclude) ) {
		$where[] = sprintf('pharmacy_id NOT IN (%s)', implode(", ", $exclude) );
	}
	$where = 'WHERE ' . implode(" AND ", $where);
	$sql = $wpdb->prepare("SELECT
		*, (
		  6371 * acos (
			cos ( radians( %f ) ) 
			* cos( radians( latitude ) )
			* cos( radians(  	longitude ) - radians( %f ) ) 
			+ sin ( radians( %f ) ) 
			* sin( radians( latitude ) )
		  )
		) AS distance
	  FROM {$wpdb->prefix}" . PHARMACY_DB_TABLE . " 
	  $where 
	  HAVING distance < %d
	  ORDER BY distance
	  LIMIT 0 , %d;", $lat, $lon, $lat, $distance, $limit );
	  	  
	  return $wpdb->get_results( $sql );
}

/**
 * @param int $post_id
 * @param WP_Post $post
 */
function pharmacy_get_the_status( $post_id = 0, $post = null ) {
    $post_id = (int) $post_id;
    
    if( $post_id && $post instanceof WP_Post ) {
        return ( $post->data->is_open() ) ? "otwarta" : "zamknięta";
    }
    
    return "zamknięta";
}

/**
 * @global WP_Post $post
 * @param int $post_id
 * @param WP_Post|null $post
 */
function pharmacy_the_status( $post_id = 0, $post = null ) {
    echo pharmacy_get_the_status( $post_id, $post );
}

/**
 * 
 * @global WP_Post $post
 * @param type $post_id
 */
function pharmacy_get_the_openings( $post_id = 0 ) {
    global $post;
    $post_id = (int) $post_id;
    
    if( !$post_id && $post instanceof WP_Post ) {
        return $post->data->get_openings();
    }
    
    return new \stdClass();
}

/**
 * 
 * @global WP_Post $post
 * @param type $post_id
 */
function pharmacy_the_openings( $post_id = 0 ) {
    return pharmacy_get_the_openings( $post_id );
}

/**
 * 
 * @return string|array
 */
function pharmacy_get_breadcrumb() {
    $crumbs = array();
    
    if( is_front_page() ) {
        return $crumbs;
    }
	
    $q_post = get_queried_object();
    
    $crumbs[] = array( 'type' => 'custom', 'url' => home_url(), 'title' => get_option( 'blogname' ) );
    
    if( is_singular('pharmacy')) {
        $pharmacy_terms = get_the_terms($q_post, 'state');
        if( $post_state = reset( $pharmacy_terms ) ) {
			$children = array();
			$other_terms = get_terms( array( 'taxonomy' => 'state', 'exclude' => array( $post_state->term_id ), 'hide_empty' => false, 'orderby' => 'name' ) );
			foreach( $other_terms as $term ) {
				$children[] = array( 'type' => 'term', 'url' => get_term_link($term), 'title' => "Apteka " . $term->name );
			}
            $crumbs[] = array( 'type' => 'term', 'url' => get_term_link($post_state), 'title' => "Apteka " . $post_state->name, 'children' => $children );
        }
        
            $pharmacy_cities =  get_the_terms($q_post, 'city');
            if( $pharmacy_cities ) {
                if( $post_city  = reset( $pharmacy_cities ) ) {
                    $crumbs[] = array( 'type' => 'term', 'url' => get_term_link($post_city), 'title' => "Apteka " . $post_city->name );
                }
            }
    }
    
    if ( is_tax('city') ) {
        if ( $city_state = get_field('state', 'city_' . $q_post->term_id) ) {
            $state_id = reset($city_state);
            $state = get_term($state_id, 'state');
            $children = array();
            $other_terms = get_terms( array( 'taxonomy' => 'state', 'exclude' => array( $state->term_id ), 'hide_empty' => false, 'orderby' => 'name' ) );
            foreach( $other_terms as $term ) {
                    $children[] = array( 'type' => 'term', 'url' => get_term_link($term), 'title' => "Apteka " .$term->name );
            }
            $crumbs[] = array( 'type' => 'term', 'url' => get_term_link($state), 'title' => "Apteka " . $state->name, 'children' => $children );
        }
    }
    
    if( is_singular('post') ) {
        $post_categories = wp_get_post_terms($q_post->ID, 'category');
        if( $post_categories ) {
            if( $post_category = reset( $post_categories ) ) {
                $children = array();
                $other_categories = get_terms( array( 'taxonomy' => 'category', 'exclude' => array( $post_category->term_id ), 'hide_empty' => false, 'orderby' => 'name' ) );
                foreach($other_categories as $term) {
                    $children[] = array( 'type' => 'term', 'url' => get_term_link($term), 'title' => $term->name );
                }
                $crumbs[] = array( 'type' => 'term', 'url' => get_term_link($post_category), 'title' => $post_category->name, 'children' => $children );
            }
        }
    }
    
    if ( $q_post instanceof WP_Post ) {
        $crumbs[] = array( 'type' => 'current', 'url' => get_permalink($q_post), 'title' => $q_post->post_title );
    } else if ( $q_post instanceof WP_Term ) {
        $crumbs[] = array( 'type' => 'current', 'url' => get_term_link($q_post), 'title' => is_category() ? $q_post->name : "Apteka " . $q_post->name );
    }
    
    return $crumbs;
}

/**
 * 
 */
function pharmacy_breadcrumb() {
    echo pharmacy_get_breadcrumb();
}

/**
 * 
 * @global wpdb() $wpdb
 * @return type
 */
function get_fulltime_pharmacies() {
    global $wpdb;
    $city = get_queried_object();
    $sql = $wpdb->prepare("SELECT p.pharmacy_id 
	  FROM {$wpdb->prefix}" . PHARMACY_DB_TABLE . " p 
          JOIN {$wpdb->term_relationships} tr ON tr.object_id = p.pharmacy_id "
          . "WHERE tr.term_taxonomy_id = {$city->term_id} "
          . "AND p.whole_day = 1");
    $pharmacies = $wpdb->get_results( $sql );
    return !is_wp_error($pharmacies) ? array_map(function($el) { return $el->pharmacy_id; }, $pharmacies ) : array();
}

function get_pharmacies_in_city() {
    global $wpdb, $wp_query;
    $city = get_queried_object();
    $posts_per_page = $wp_query->query_vars['posts_per_page'];
    $page = isset($wp_query->query['paged']) ? (int) $wp_query->query['paged'] : 1;

    $sql_correct_sorting_without_posts = 'SELECT * 
FROM o13k4_pharmacy_openings pho 
LEFT JOIN o13k4_term_relationships tr ON (pho.pharmacy_id = tr.object_id) 
LEFT JOIN o13k4_pharmacy ph ON (ph.pharmacy_id = pho.pharmacy_id)  
WHERE 1=1 
AND ( tr.term_taxonomy_id IN (' . $city->term_id . ') ) 
AND ( ph.whole_day = 0 )
ORDER BY 
	CASE
		WHEN day-1 >= WEEKDAY(CURDATE()) AND close != \'00:00\' AND CONVERT(close,TIME) < CURRENT_TIME THEN day+7 
		WHEN day-1 >= WEEKDAY(CURDATE()) THEN day 
        WHEN day-1 < WEEKDAY(CURDATE()) THEN day+7 
    END ASC,
CONVERT(open ,TIME) ASC,
CONVERT(close, TIME) DESC';

    $wpdb->query('SET time_zone=\'+02:00\';');
    $pharmacy_results = $wpdb->get_results($sql_correct_sorting_without_posts);

    $pharmacies = $pharmacies_ids = [];

    foreach ($pharmacy_results as $pharmacy) {
        if (!in_array($pharmacy->pharmacy_id, $pharmacies_ids)) {
            $pharmacies_ids[] = $pharmacy->pharmacy_id;
            $data = get_post($pharmacy->pharmacy_id);
            $data->data = \Pharmacy::get_instance( $pharmacy->pharmacy_id );

            array_push($pharmacies, $data);
        }
    }

    $args = array(
        'post_type' => 'pharmacy',
        'post__not_in' => $pharmacies_ids,
        'posts_per_page' => -1,
        'tax_query' => array(
            array(
                'taxonomy' => $city->taxonomy,
                'field'    => 'id',
                'terms'    => $city->term_id,
            ),
        ),
    );
    $queries = new WP_Query($args);
    if ($queries->have_posts()) {
        foreach ($queries->get_posts() as $query_post) {
            $query_post->data = \Pharmacy::get_instance( $query_post->ID );
            if (!$query_post->data->is_fulltime()) {
                array_push($pharmacies, $query_post);
            }
        }
    }

    $wp_query->max_num_pages = ceil(count($pharmacies) / $posts_per_page);

    $pagedArray = array_chunk($pharmacies, $posts_per_page);

    return $pagedArray[$page-1];
}

/**
 * @return array|void
 */
function get_nearest_pharmacies() {
    global $post;

    if (!$post instanceof \WP_Post || !is_singular('pharmacy')) {
        return;
    }

    $lat = $post->data->latitude;
    $lng = $post->data->longitude;
    $distance = 5;

    if( isset($_COOKIE['currentUserLatLng']) ) {
        $user_location = urldecode($_COOKIE['currentUserLatLng']);
        list($lat, $lng) = explode(";", $user_location);
    }

    $nearest_pharmacies = get_nearest_pharmacies_by_lat_lon($lat, $lng, array($post->ID), $distance, 20 );
    set_query_var('current_nearest_pharmacies', $nearest_pharmacies);
    set_query_var('current_nearest_pharmacies_distance', $distance);

    return $nearest_pharmacies;
}
