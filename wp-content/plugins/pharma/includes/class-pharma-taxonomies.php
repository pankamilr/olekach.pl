<?php
/**
 * @author Kamil Ryczek <kamilk@piszekod.pl>
 * @copyright (c) 2019, O Lekach
 * @package olekach.pl
 * 
 * Custom taxonomies
 * Fill $taxes array with your custom taxonomies slugs
 */

if (!class_exists('Pharma_Taxonomies')) {
	
	class Pharma_Taxonomies {

		/**
		 * array(term => array(post_type))
		 */
		var $taxonomies = array();

		public function __construct($taxonomies = false) {
			if($taxonomies && is_array($taxonomies)) {
				$this->taxonomies = $taxonomies;
			}
                        add_action('wp_ajax_nopriv_search_terms', array($this, 'search_terms') );
                        add_action('wp_ajax_search_terms', array($this, 'search_terms') );

			add_action('init', array($this, 'register_tax'));
		}
                
                public function search_terms() {
                    $term_field = sanitize_text_field($_REQUEST['field']);
                    $term_value = sanitize_text_field($_REQUEST['lookup']);
                    $term_taxonomy = sanitize_key($_REQUEST['taxonomy']);
                    
                    if(empty($term_field)) {
                        $term_field = "name";
                    }
                    
                    if( !empty($term_value) || !empty($term_taxonomy) ) {
                        $terms = get_terms(array(
                            'taxonomy' => $term_taxonomy,
                            'hide_empty' => false,
                            $term_field => $term_value
                        ));
                        
                        if( !is_wp_error($terms) ) {
                            header('Content-Type: application/json');
                            $lookup_term = array_shift($terms);
                            $lookup_term->url = get_term_link($lookup_term);
                            echo wp_json_encode( $lookup_term );
                            exit();
                        }
                    }
                    
                    echo wp_json_encode( array() );
                    exit();
                }

		/**
		 * Wywołanie funkcji register_post_type w pętli 
		 * z zadeklarowanymi nazwami rodzajów zawartości
		 */
		public function register_tax() {
			foreach ($this->taxonomies as $tax => $args) {
				register_taxonomy($tax, $args['types'], call_user_func_array(array($this, "tax_args"), array($tax)));
			}
		}

		/**
		 * Ustawienia argumentów rodzajów zawartości
		 * 
		 * @param string $type
		 * @return array 
		 */
		protected function tax_args($type) {
			/**
			 * Zmienna przetrzymująca dedykowane ustawienia dla typu
			 * Zerowanie zmiennej
			 */
			$type_args = NULL;

			/**
			 * Standardowe ustawienia
			 */
			$args = array(
				'hierarchical' => true,
				'labels' => call_user_func_array(array($this, "tax_labels"), array($type)),
				'public' => true,
				'show_ui' => true,
				'show_admin_column' => true,
				'update_count_callback' => '_update_post_term_count',
				'query_var' => true,
			);


			/**
			 * Dedykowane ustawienia dla danego typu $type
			 * @param $type string 
			 * @use $type_args
                         * 
			 * Scalanie standardowych ustawień z dedykowanymi
			 * Dedykowane klucze tablicy nadpisuja standardowe ustawienia
			 * Klucze nie wypisane w dedykowanej tablicy zostają bez zmian
			 */
                        if(isset($this->taxonomies[$type]) && isset($this->taxonomies[$type]['args']) && is_array($this->taxonomies[$type]['args'])) {
				$args = array_merge($args, $this->taxonomies[$type]['args']);
			}

			return $args;
		}

		/**
		 * Ustawienia labelów, opisów dla rodzajów zawartości
		 * 
		 * @param string $type
		 * @return array
		 */
		protected function tax_labels($type) {
			/**
			 * Zmienna przetrzymująca dedykowane ustawienia dla typu
			 * Zerowanie zmiennej
			 */
			$type_labels = NULL;

			/**
			 * Standardowe ustawienia
			 * Nazwa wyświetlana generowana na podstawie nazwy bazowej rodzaju zawartości
			 */
			$default_name = str_replace(array("_", "-"), array(" ", " "), $type);
			$default_name = ucwords($default_name);

			$labels = array(
				'name' => $default_name,
				'singular_name' => $default_name,
				'menu_name' => $default_name,
				'name_admin_bar' => $default_name
			);

			/**
			 * Dedykowane ustawienia dla danego typu $type
			 * @param $type string
			 * 
			 * Scalanie standardowych ustawień z dedykowanymi
			 * Dedykowane klucze tablicy nadpisuja standardowe ustawienia
			 * Klucze nie wypisane w dedykowanej tablicy zostają bez zmian
			 */
                        if(isset($this->taxonomies[$type]) && isset($this->taxonomies[$type]['labels']) && is_array($this->taxonomies[$type]['labels'])) {
				$labels = array_merge($labels, $this->taxonomies[$type]['labels']);
			}

			return $labels;
		} 
	}
	
}
