<?php

/**
 * @author Kamil Ryczek <kamil.ryczek@artefakt.pl>
 * @copyright (c) 2019, Artefakt
 * @package olekach.pl
 */

class Pharma {
	
	private static $initiated = false;
	
	public static function init() {
		if ( ! self::$initiated ) {
			self::init_hooks();
		}
	}
	
	public static function init_hooks() {
		
		self::$initiated = true;
		
		add_action( 'init', array( 'Pharma', 'register_post_type' ), 8 );
		add_action( 'init', array( 'Pharma', 'register_taxonomy' ), 9 );
		add_action( 'widgets_init', array( 'Pharma', 'register_widgets' ) );
		add_filter( 'post_type_link', array( 'Pharma', 'overwrite_post_type_link'), 10, 2);		
		add_filter( 'post_link', array( 'Pharma', 'overwrite_post_link'), 10, 2);		
		add_action( 'admin_menu', array(new Pharma_CLI_Settings_Page(), 'register_page') );
                add_action( 'the_post', array( 'Pharma', 'the_post' ), 10, 2 );
                add_action( 'init', array( 'Pharma', 'add_rewrite_rules'), 999 );  
                add_filter( 'navigation_markup_template', array('Pharma', 'navigation_markup_template'), 10, 2);
				
//                add_filter( 'posts_where', array( 'Pharma', 'posts_where' ), 10, 2 );
//                add_filter( 'posts_orderby', array( 'Pharma', 'posts_orderby' ), 10, 2 );
//                add_filter( 'posts_groupby', array( 'Pharma', 'posts_groupby' ), 10, 2 );
//                add_filter( 'posts_join', array( 'Pharma', 'posts_join' ), 10, 2 );
//                add_filter( 'posts_request', array( 'Pharma', 'posts_request'), 10, 2);
                add_action( 'pre_get_posts', array( 'Pharma', 'pre_get_posts' ) );
                                
                add_filter( 'show_admin_bar',   array('Pharma', 'admin_bar'));
                add_filter( 'tiny_mce_plugins', array('Pharma', 'disable_emojis_tinymce'));
                add_action( 'init',             array('Pharma', 'disable_emojis'));
                add_action( 'init',             array('Pharma', 'head_removal'));
                add_filter( 'wpseo_robots',     array('Pharma', 'wpseo_robots') );
                add_action( 'wpseo_register_extra_replacements', array('Pharma', 'wpseo_register_extra_replacements') );
                add_action( 'save_post_post',   array('Pharma', 'save_post_post'), 10, 3 );
                
                //add_filter( 'the_posts', array('Pharma', 'the_posts'), 10, 2);
                add_filter( 'style_loader_tag', array('Pharma', 'style_loader_tag'), 10, 2);
        
	}
        

        public static function style_loader_tag($html, $handle) {
            if($handle === 'roboto-font-file') {
                return str_replace('rel="stylesheet"', 'rel="preload" as="font" type="font/woff2" crossorigin="anonymous" />', $html);
            }
            return $html;
        }
        
        public static function the_posts(array $posts, \WP_Query $query) {
            $unique_results = [];
            
            foreach ($posts as $post) {
                if (!isset($unique_results[$post->ID])) {
                    $unique_results[$post->ID] = $post;
                }
            }
            
            return array_values($unique_results);
        }
        
        /**
         * 
         * @return string
         */
        public static function wpseo_replace_pharmacy_address() {
            global $post;
            if( $post instanceof \WP_Post ) {
                if( !isset($post->data) ) {
                    $post->data = \Pharmacy::get_instance( $post->ID );
                }
                return ", " . $post->data->get_address('array')[0];
            }
            return "";
        }
        
        /**
         * 
         */
        public static function wpseo_register_extra_replacements() {
            wpseo_register_var_replacement( '%%pharmacyaddress%%', array('Pharma', 'wpseo_replace_pharmacy_address'), 'advanced', 'some help text' );
        }
	
        /**
         * 
         * @param type $post_id
         * @param type $post
         * @param type $update
         * @return type
         */
        public static function save_post_post($post_id, $post, $update) {
                // If this is just a revision, don't send the email.
            if ( wp_is_post_revision( $post_id ) || $update ) {
                return;
            }
            
            update_post_meta( $post_id, 'wp_review_user_reviews', 5 );
            update_post_meta( $post_id, 'wp_review_review_count', rand(1, 20) );
            update_post_meta( $post_id, 'wp_review_negative_count', 0 );
            update_post_meta( $post_id, 'wp_review_positive_count', 0 );
            
        }
        
		public static function wpseo_robots( $robotsstr ) {
			if ( is_paged() ) {
				return 'noindex,follow';		
			}
			return $robotsstr;
		}
        
        public static function pre_get_posts(\WP_Query $query) {
            if ( $query->is_search() ) {
                $query->set( 'post_type', array( 'pharmacy', 'post' ) );
            }

            if ($query->is_category()) {
                $query->set('posts_per_page', 10);
            }
            
//            if ($query->is_tax('city')) {
//                $query->set('posts_per_page', -1);
//            }
        }
	
	public static function posts_join( $join, \WP_Query $query ) {
		if( $query->is_tax('city') || $query->is_tax('state') ) {
			global $wpdb;
			$join .= " LEFT JOIN {$wpdb->prefix}" . PHARMACY_DB_TABLE . " ON ({$wpdb->posts}.ID = {$wpdb->prefix}" . PHARMACY_DB_TABLE . ".pharmacy_id)";
			$join .= " LEFT JOIN {$wpdb->prefix}" . PHARMACY_OPENINGS_DB_TABLE . " ON ({$wpdb->posts}.ID = {$wpdb->prefix}" . PHARMACY_OPENINGS_DB_TABLE . ".pharmacy_id)";
		}
		return $join;
	}
	
	public static function posts_where( $where, \WP_Query $query ) {
		if( $query->is_tax('city') || $query->is_tax('state') ) {
                    global $wpdb;
                    $where .= " AND {$wpdb->prefix}" . PHARMACY_DB_TABLE . ".whole_day = 0";
		}
		return $where;
	}
	
	public static function posts_orderby( $orderby, \WP_Query $query ) {
		if( $query->is_tax('city') || $query->is_tax('state') ) {
			$current_day = date("N");
			$range = array_merge( $current_day < 7 ? range($current_day+1, 7) : array(), range(1, $current_day) );
			
			global $wpdb;                        
			$orderby = "FIELD({$wpdb->prefix}" . PHARMACY_OPENINGS_DB_TABLE . ".day, " . implode(", ", $range) . ") DESC, '" . date("H:i") . "' COLLATE utf8mb4_unicode_520_ci NOT BETWEEN CONCAT( LPAD( {$wpdb->prefix}" . PHARMACY_OPENINGS_DB_TABLE . ".open, 2, '0'), ':00') AND CONCAT( LPAD( {$wpdb->prefix}" . PHARMACY_OPENINGS_DB_TABLE . ".close, 2, '0'), ':00'), 
                            LPAD( {$wpdb->prefix}" . PHARMACY_OPENINGS_DB_TABLE . ".close, 2, '0' ), LPAD( {$wpdb->prefix}" . PHARMACY_OPENINGS_DB_TABLE . ".open, 2, '0' ), {$wpdb->posts}.post_title ASC";
		}
		return $orderby;
	}
	
	public static function posts_groupby( $groupby, \WP_Query $query ) {
		if( $query->is_tax('city') || $query->is_tax('state') ) {
                    global $wpdb;
//                    $groupby .= ", {$wpdb->prefix}" . PHARMACY_OPENINGS_DB_TABLE . ".day";
//                    $groupby .= ", {$wpdb->prefix}" . PHARMACY_OPENINGS_DB_TABLE . ".open";
                    $groupby .= ", {$wpdb->prefix}" . PHARMACY_OPENINGS_DB_TABLE . ".pharmacy_id";
                }
		return $groupby;
	}
        
        public static function posts_request($pieces) {
            return $pieces;
        }
        
        public static function navigation_markup_template($template, $class) {
            return '<nav class="navigation row bg-white mx-0 rounded-0 py-3 d-flex justify-content-center %1$s" role="navigation" data-title="%2$s">
		<div class="nav-links">%3$s</div>
                </nav>';
        }
	
        public static function add_rewrite_rules() {
            add_rewrite_rule(
                'blog/([^/]*)$',
                'index.php?name=$matches[1]',
                'top'
            );
            add_rewrite_tag('%blog%','([^/]*)');
        }

	public static function register_widgets() {
		register_widget( 'Pharma_Posts_Read_More_Widget' );
		register_widget( 'Pharma_Pharmacies_More_Widget' );
		register_widget( 'Pharma_Nearest_Pharmacies_Widget' );
		register_widget( 'Pharma_Cities_In_State_Widget' );
		register_widget( 'Pharma_Cities_Pharmacies_Map_Widget' );
		register_widget( 'Pharma_Synonymous_Text_Widget' );
	}
        
        public static function the_post($post, $query) {            
            $post->data = \Pharmacy::get_instance( $post->ID );
        }

	/**
	 * 
	 * @param type $post_link
	 * @param type $post
	 * @param type $leavename
	 * @param type $sample
	 * @return type
	 */
	public static function overwrite_post_type_link($post_link, $post) {
		if ( $post->post_type === 'pharmacy' && false !== strpos( $post_link, '%category%' ) ) {
			$pharmacy_city_term = get_the_terms( $post->ID, 'city' );
                        if($pharmacy_city_term) {
                            $post_link = str_replace( '%category%', array_pop( $pharmacy_city_term )->slug, $post_link );
                        }
		}
                
                return $post_link;
	}

	/**
	 * 
	 * @param type $post_link
	 * @param type $post
	 * @param type $leavename
	 * @param type $sample
	 * @return type
	 */
	public static function overwrite_post_link($post_link, $post) {		
                if ( $post->post_type != 'post' ) {
                    return $post_link;
                }
                
                if ( false !== strpos( $post_link, '%postname%' ) ) {
                    $slug = '%postname%';
                }
                elseif ( $post->post_name ) {
                    $slug = $post->post_name;
                }
                else {
                    $slug = sanitize_title( $post->post_title );
                }

                $post_link = home_url( user_trailingslashit( 'blog/'. $slug ) );
                return $post_link;
	}
	
	/**
	 * 
	 */
	public static function register_post_type() {
		new Pharma_Post_Types(array(
			'pharmacy' => array(
				'args' => array(
					'has_archive' => 'apteki',
					'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
					'rewrite' => array('slug' => 'apteka/%category%'),
					'menu_icon' => 'dashicons-store',
					'taxonomies' => array('city', 'state'),
				),
				'labels' => array(
					'name' => 'Apteki',
					'singular_name' => 'Apteka',
					'menu_name' => 'Apteki'
				)
			),
			'settings' => array(
				'args' => array(
					'has_archive' => false,
					'supports' => array('title'),
					'rewrite' => false,
					'public' => false,
					'show_ui' => true,
					'show_in_menu' => false,
					'show_in_nav_menus' => false,
				)
			)
		));
	}
	
	/**
	 * 
	 */
	public static function register_taxonomy() {
		new Pharma_Taxonomies(array(
			'city' => array(
				'types' => array('pharmacy'),
				'args' => array(
					'hierarchical' => false,
					'show_admin_column' => true,
					'rewrite' => array(
						'slug' => 'apteki',
						'with_front' => true
					),
					'query_var' => 'apteki'
				),
				'labels' => array(
					'name' => 'Miasta',
					'singular_name' => 'Miasto',
					'menu_name' => 'Miasta',
				)
			),
			'state' => array(
				'types' => array('pharmacy', 'city'),
				'args' => array(
					'hierarchical' => false,
					'show_admin_column' => true,
					'rewrite' => array(
						'slug' => 'apteki/wojewodztwo',
						'with_front' => true
					),
//					'query_var' => 'apteki'
				),
				'labels' => array(
					'name' => 'Województwa',
					'singular_name' => 'Województwo',
					'menu_name' => 'Województwa',
				)
			)
		));
	}
        
        
    /**
     * Remove admin bar
     * @return boolean
     */
    public static function admin_bar(){
        return false;
    }
    
    /**
     * Disable the emoji's
     * From Plugin: Disable Emojis
     * Plugin URI: https://geek.hellyer.kiwi/plugins/disable-emojis/
     * Author: Ryan Hellyer
     * Author URI: https://geek.hellyer.kiwi/
     */
    public static function disable_emojis() {
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('admin_print_scripts', 'print_emoji_detection_script');
        remove_action('wp_print_styles', 'print_emoji_styles');
        remove_action('admin_print_styles', 'print_emoji_styles');
        remove_filter('the_content_feed', 'wp_staticize_emoji');
        remove_filter('comment_text_rss', 'wp_staticize_emoji');
        remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    }

    /**
     * Filter function used to remove the tinymce emoji plugin.
     * 
     * @param    array  $plugins  
     * @return   array             Difference betwen the two arrays
     */
    public static function disable_emojis_tinymce($plugins) {
        if (is_array($plugins)) {
            return array_diff($plugins, array('wpemoji'));
        } else {
            return array();
        }
    }
    
    /**
     * Removes messy code from wp_head()
     */
    public static function head_removal() {        
        remove_action('wp_head', 'rsd_link');
        remove_action('wp_head', 'wp_generator');
        remove_action('wp_head', 'feed_links', 2);
        remove_action('wp_head', 'index_rel_link');
        remove_action('wp_head', 'wlwmanifest_link');
        remove_action('wp_head', 'feed_links_extra', 3);
        remove_action('wp_head', 'start_post_rel_link', 10, 0);
        remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
        remove_action('wp_head', 'parent_post_rel_link', 10, 0);
        remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
        remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
    }
    

}
