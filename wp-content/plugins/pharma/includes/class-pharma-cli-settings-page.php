<?php

/**
 * @author Kamil Ryczek <kamil@piszekod.pl>
 * @copyright (c) 2019, O Lekach
 * @package olekach.pl
 */

/**
 * Manage custom settings page 
 */
class Pharma_CLI_Settings_Page {
	
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;
	
	private $extensions = array('.csv' => 'CSV', '.xlsx' => 'XLSX', '.xml' => 'XML');
	
	public function __construct() {
		add_action( 'admin_init', array( $this, 'register_settings' ) );
	}
	
	/**
	 * Register new submenu page under Tools menu-tab
	 */
	public function register_page() {
		add_management_page( "Pharma CLI Settings", "Pharma CLI", 'activate_plugins', "pharma-cli-settings", array( $this, 'callback') );
	}
	
	public function register_settings() {
		register_setting(
            'pharma_cli_settings',
            'pharma_cli_settings',
            array( $this, 'sanitize' )
        );

        add_settings_section(
            'file_section_id',
            'Ustawienia pobierania',
            array( $this, 'print_section_info' ),
            'pharma-cli-settings'
        );  

        add_settings_field(
            'url',
            'File URL',
            array( $this, 'url_callback' ),
            'pharma-cli-settings',
            'file_section_id'        
        );      

        add_settings_field(
            'extension', 
            'Select file extension', 
            array( $this, 'extension_callback' ), 
            'pharma-cli-settings', 
            'file_section_id'
        );      

        add_settings_field(
            'current_file', 
            'Latest downloaded file', 
            array( $this, 'current_file_callback' ), 
            'pharma-cli-settings', 
            'file_section_id'
        );      
	}
	
	/**
	 * Display settings page template
	 */
	public function callback() {
        $this->options = get_option( 'pharma_cli_settings' );
		include_once PHARMA_CLI_DIR . '/template-parts/settings-page.php';
	}
	
    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();
        if( isset( $input['extension'] ) )
            $new_input['extension'] = sanitize_text_field( $input['extension'] );

        if( isset( $input['url'] ) )
            $new_input['url'] = sanitize_text_field( $input['url'] );

        if( isset( $input['current_file'] ) )
            $new_input['current_file'] = sanitize_text_field( $input['current_file'] );

        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Podaj wymagane dane:';
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function extension_callback()
    {
		$options = "";
		foreach($this->extensions as $ext => $label) {
			$selected = isset( $this->options['extension'] ) ? selected(esc_attr( $this->options['extension']), $ext, false) : '';
			$options .= sprintf('<option value="%s"%s>%s</option>', $ext, $selected, $label);
		}
        printf(
            '<select id="extension" name="pharma_cli_settings[extension]">%s</select>',
			$options            
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function url_callback()
    {
        printf(
            '<input type="url" id="url" class="regular-text code" name="pharma_cli_settings[url]" value="%s" />',
            isset( $this->options['url'] ) ? esc_attr( $this->options['url']) : ''
        );
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function current_file_callback()
    {
        printf(
            '<input type="text" id="current_file" readonly disabled class="regular-text code" name="pharma_cli_settings[current_file]" value="%s" />',
            isset( $this->options['current_file'] ) ? esc_attr( $this->options['current_file']) : ''
        );
    }
	
}