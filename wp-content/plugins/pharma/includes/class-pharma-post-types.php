<?php

/**
 * @author Kamil Ryczek <kamilk@piszekod.pl>
 * @copyright (c) 2019, O Lekach
 * @package olekach.pl
 */

if ( !class_exists('Pharma_Post_Types')) {
	
	class Pharma_Post_Types {

		/**
		 * Rodzaje zawartości do stworzenia
		 * @var type 
		 */
		var $types = array();

		/**
		 * Dodanie akcji rejestrującej rodzaje zawartości
		 */
		function __construct($types = false) {
			if($types && is_array($types)) {
				$this->types = $types;
			}

			add_action("init", array($this, "register_types"));
		}

		/**
		 * Wywołanie funkcji register_post_type w pętli 
		 * z zadeklarowanymi nazwami rodzajów zawartości
		 */
		public function register_types() {
			foreach ($this->types as $cpt => $args) {
				register_post_type($cpt, call_user_func_array(array($this, "types_args"), array($cpt)));
			}
		}

		/**
		 * Ustawienia argumentów rodzajów zawartości
		 * 
		 * @param string $type
		 * @return array 
		 */
		protected function types_args($type) {
			/**
			 * Zmienna przetrzymująca dedykowane ustawienia dla typu
			 * Zerowanie zmiennej
			 */
			$type_args = NULL;

			/**
			 * Standardowe ustawienia
			 */
			$args = array(
				'labels' => call_user_func_array(array($this, "types_labels"), array($type)),
				'public' => true,
				'publicly_queryable' => true,
				'show_ui' => true,
				'show_in_menu' => true,
				'show_in_nav_menus' => true,
				'query_var' => true,
				'rewrite' => false,
				'capability_type' => 'post',
				'has_archive' => false,
				'hierarchical' => false,
				'menu_position' => null,
				'supports' => array('title', 'editor', 'excerpt')
			);

			/**
			 * Dedykowane ustawienia dla danego typu $type
			 * @param $type string 
			 * 
			 * Scalanie standardowych ustawień z dedykowanymi
			 * Dedykowane klucze tablicy nadpisuja standardowe ustawienia
			 * Klucze nie wypisane w dedykowanej tablicy zostają bez zmian
			 */
			if(isset($this->types[$type]) && isset($this->types[$type]['args']) && is_array($this->types[$type]['args'])) {
				$args = array_merge($args, $this->types[$type]['args']);
			}

			return $args;
		}

		/**
		 * Ustawienia labelów, opisów dla rodzajów zawartości
		 * 
		 * @param string $type
		 * @return array
		 */
		protected function types_labels($type) {
			/**
			 * Zmienna przetrzymująca dedykowane ustawienia dla typu
			 * Zerowanie zmiennej
			 */
			$type_labels = NULL;

			/**
			 * Standardowe ustawienia
			 * Nazwa wyświetlana generowana na podstawie nazwy bazowej rodzaju zawartości
			 */
			$default_name = str_replace(array("_", "-"), array(" ", " "), $type);
			$default_name = ucwords($default_name);

			$labels = array(
				'name' => $default_name,
				'singular_name' => $default_name,
				'menu_name' => $default_name,
				'name_admin_bar' => $default_name
			);

			/**
			 * Dedykowane ustawienia dla danego typu $type
			 * @param $type string 
			 * 
			 * Scalanie standardowych ustawień z dedykowanymi
			 * Dedykowane klucze tablicy nadpisuja standardowe ustawienia
			 * Klucze nie wypisane w dedykowanej tablicy zostają bez zmian
			 */		
			if(isset($this->types[$type]) && isset($this->types[$type]['labels']) && is_array($this->types[$type]['labels'])) {
				$labels = array_merge($labels, $this->types[$type]['labels']);
			}
			
			return $labels;
		}

	}
	
}
