<?php

/**
 * @author Kamil Ryczek <kamil.ryczek@artefakt.pl>
 * @copyright (c) 2019, Artefakt
 * @package pinia.pl
 */


$argv = array( "", "_metadata/Rejestr Aptek_stan na dzien 20190915090205.xml");

if(empty($argv[1]))
{
	die("Please specify xml file to parse.\n");
}
 
$countIx = 0;
 
$xml = new XMLReader();
$xml->open($argv[1]);

while($xml->read() && $xml->name != 'Apteka')
{
	;
}
 
while($xml->name == 'Apteka')
{
	$element = new SimpleXMLElement($xml->readOuterXML());
	
	$prod = array(
		'kodPocztowy' => strval($element->Adres->attributes()->kodPocztowy),
		'miejscowosc' => strval($element->Adres->attributes()->miejscowosc),
		'typUlicy' => strval($element->Adres->attributes()->typUlicy),
		'nazwaUlicy' => strval($element->Adres->attributes()->nazwaUlicy),
		'numerDomu' => strval($element->Adres->attributes()->numerDomu),
		'numerLokalu' => strval($element->Adres->attributes()->numerLokalu),
		'poczta' => strval($element->Adres->attributes()->poczta),
		'wojewodztwo' => strval($element->Adres->attributes()->wojewodztwo),
		'DniPracyApteki' => $element->DniPracyApteki->DniPracy->children(),
	);
	
	print_r($prod);
	exit;
	print "\n";
	$countIx++;
	
	$xml->next('Apteka');
	unset($element);
}
 
print "Number of items=$countIx\n";
print "memory_get_usage() =" . memory_get_usage()/1024 . "kb\n";
print "memory_get_usage(true) =" . memory_get_usage(true)/1024 . "kb\n";
print "memory_get_peak_usage() =" . memory_get_peak_usage()/1024 . "kb\n";
print "memory_get_peak_usage(true) =" . memory_get_peak_usage(true)/1024 . "kb\n";
 
print "custom memory_get_process_usage() =" . memory_get_process_usage() . "kb\n";
 
 
$xml->close();
 
/**
 * Returns memory usage from /proc<PID>/status in kbytes.
 *
 * @return int|bool sum of VmRSS and VmSwap in kbytes. On error returns false.
 */
function memory_get_process_usage()
{
	$status = file_get_contents('/proc/' . getmypid() . '/status');
	
	$matchArr = array();
	preg_match_all('~^(VmRSS|VmSwap):\s*([0-9]+).*$~im', $status, $matchArr);
	
	if(!isset($matchArr[2][0]) || !isset($matchArr[2][1]))
	{
		return false;
	}
	
	return intval($matchArr[2][0]) + intval($matchArr[2][1]);
}

exit;


