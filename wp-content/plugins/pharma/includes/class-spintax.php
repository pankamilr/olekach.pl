<?php
/**
 * Spintax - A helper class to process Spintax strings.
 * @name Spintax
 * @author Jason Davis - https://www.codedevelopr.com/
 * Tutorial: https://www.codedevelopr.com/articles/php-spintax-class/
 */

class Spintax
{
    public static function process($text)
    {
        return preg_replace_callback(
            '/\{(((?>[^\{\}]+)|(?R))*)\}/x',
            array('Spintax', 'replace'),
            $text
        );
    }
    public static function replace($text)
    {
        $text = self::process($text[1]);
        $parts = explode('|', $text);
        return $parts[array_rand($parts)];
    }
}
