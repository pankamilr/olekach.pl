<?php

/**
 * @author Kamil Ryczek <kamil.ryczek@artefakt.pl>
 * @copyright (c) 2019, Artefakt
 * @package olekach.pl
 */

class Pharma_CLI_DB {
	
	public static function activation() {
		global $wpdb;	
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

		$charset_collate = $wpdb->get_charset_collate();
		$version         = (int) get_option( 'pharma_cli_db_version' );

		if ( $version < 3 ) {
			$table_name = $wpdb->prefix.PHARMACY_DB_TABLE;
			$sql = "CREATE TABLE `$table_name` (
				pid bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
				pharmacy_id bigint(20) UNSIGNED NOT NULL,
				internal_id bigint(20) UNSIGNED NOT NULL comment 'wewnetrzne id',
				created_at datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
				updated_at datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
				status boolean comment 'status apteki',
				name varchar(255) NOT NULL comment 'nazwa apteki',
				website varchar(255) comment 'strona www',
				phone varchar(255) comment 'numer telefonu',
				postcode varchar(10) comment 'kod pocztowy',
				city varchar(100) comment 'miasto',
				street_type varchar(30) comment 'typ ulicy',
				street varchar(200) comment 'ulica',
				house varchar(100) comment 'numer domu',
				apartment varchar(100) comment 'numer lokalu',
				post varchar(100) comment 'poczta',
				state varchar(100) comment 'wojewodztwo',
				county varchar(100) comment 'powiat',
				community varchar(100) comment 'gmina',
				ulic_id varchar(20),
				simc_id varchar(20),
				terc_id varchar(20),
				whole_day boolean comment 'otwarta 24h',
				latitude varchar(100) comment 'szerokosc geograficzna',
				longitude varchar(100) comment 'dlugosc geograficzna',
			PRIMARY KEY (pid),
			KEY pharmacy_id (pharmacy_id),
			KEY pharmacy_city (pharmacy_id, city),
			KEY pharmacy_geotag (pharmacy_id, latitude, longitude),
			KEY city (city),
			KEY name (name),
			CONSTRAINT fk_phramacies_post_id FOREIGN KEY (pharmacy_id) REFERENCES `{$wpdb->posts}` (ID) ON DELETE CASCADE ON UPDATE NO ACTION
			) $charset_collate;";
			
			$table_name = $wpdb->prefix.PHARMACY_OPENINGS_DB_TABLE;
			$sql .= "CREATE TABLE `$table_name` (
				poid bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
				pharmacy_id bigint(20) UNSIGNED NOT NULL,
				created_at datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
				updated_at datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
				day smallint,
				open varchar(5),
				close varchar(5),
				fulltime boolean comment 'otwarta 24',
			PRIMARY KEY  (poid),
			KEY pharmacy_id (pharmacy_id),
			KEY pharmacy_day (pharmacy_id, day),
			KEY day_open_close (day,open,close),
			CONSTRAINT fk_openings_post_id FOREIGN KEY (pharmacy_id) REFERENCES `{$wpdb->posts}` (ID) ON DELETE CASCADE ON UPDATE NO ACTION
			) $charset_collate;";

			dbDelta($sql); 
			$success = empty( $wpdb->last_error );

			update_site_option( 'pharma_cli_db_version', 3 );
			return $success;
		}
	}
	
	public static function deactivation() {
		
	}
	
	public static function uninstall() {
		
	}
        
        public static function get_pharmacy_data(int $post_id = 0) {
            if( !$post_id ) {
                return null;
            }
            
            global $wpdb;
            $sql = $wpdb->prepare("SELECT * 
            FROM {$wpdb->prefix}" . PHARMACY_DB_TABLE . "  
            WHERE pharmacy_id = %d
            LIMIT 0 , 1;", $post_id );
            
            $pharmacy_data = $wpdb->get_row( $sql );
            
            $sql = $wpdb->prepare("SELECT * 
            FROM {$wpdb->prefix}" . PHARMACY_OPENINGS_DB_TABLE . "  
            WHERE pharmacy_id = %d;", $post_id );
            $openings = $wpdb->get_results( $sql );
            
			$number = date('N');
			$days = new \stdClass();
			foreach($openings as $day) {
				$days->{$day->day}			= new \stdClass();
				$days->{$day->day}->open	= $day->open;
				$days->{$day->day}->close	= $day->close;
				$days->{$day->day}->all_day	= $day->fulltime;
				
				$datetime = new \DateTime();
				$datetime->setTimezone( new \DateTimeZone('Europe/Warsaw') );
				$datetime->modify('Sunday this week')->modify( $day->day . " days" );
				$days->{$day->day}->day	= mysql2date('l', $datetime->format('Y-m-d'));
				
			}
			$pharmacy_data->openings = $days;
                        $pharmacy_data->today = $pharmacy_data->openings->{date("N")};
            return $pharmacy_data;
        }
	
}
