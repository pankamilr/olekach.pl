<?php

/**
 * @author Kamil Ryczek <kamil@piszekod.pl>
 * @copyright (c) 2019, O Lekach
 * @package olekach.pl
 * 
 * Crontab schema
 * freq			/path_to_wpcli		--path=path_to_correct_wordpress	commands
 * 0 * * * *	/usr/local/bin/wp	--path=/var/www/html				aco-stats update
 */
class Import_Pharmacy_Data_CLI {

    /**
     *
     * @var type 
     */
    private $data_path = PHARMA_CLI_DIR . '/data';
    private $days_of_week = array(
        'poniedzialek' => 1,
        'wtorek' => 2,
        'sroda' => 3,
        'czwartek' => 4,
        'piatek' => 5,
        'sobota' => 6,
        'niedziela' => 7
    );

    public function __construct() {
        define('WP_IMPORTING', true);
        remove_action('do_pings', 'do_all_pings', 10, 1);
    }

    /**
     * 
     */
    public function locate_pharmacies() {
        global $wpdb;
        $results = $wpdb->get_results("SELECT pid, street, city, postcode, house, apartment FROM {$wpdb->prefix}" . PHARMACY_DB_TABLE . " WHERE latitude IS NULL");

        if ($results) {

            ignore_user_abort(true);
            wp_defer_term_counting(true);
            wp_defer_comment_counting(true);

            global $wpdb;
            $wpdb->query('SET autocommit = 0;');

            foreach ($results as $result) {
                $formatted_address = sprintf("%s %s, %s", !empty($result->street) ? $result->street : $result->city, $result->house, $result->city);

                $search_url = sprintf("https://nominatim.openstreetmap.org/search?q=%s&format=json&polygon=0&addressdetails=1", urlencode($formatted_address));

                /** @var WP_Http::request() $request */
                $request = wp_safe_remote_get($search_url);

                if (!is_wp_error($request)) {
                    $response = json_decode($request['body']);
                    if (!empty($response)) {
                        $update = $wpdb->update($wpdb->prefix . PHARMACY_DB_TABLE,
                                array(
                                    'latitude' => $response[0]->lat,
                                    'longitude' => $response[0]->lon
                                ),
                                array('pid' => $result->pid)
                        );
                    }
                }
                sleep(1);
            }

            $wpdb->query('COMMIT;');

            wp_defer_term_counting(false);
            wp_defer_comment_counting(false);
        }
    }

    /**
     * 
     */
    public function debug_data() {
        $settings = get_option(PHARMA_CLI_SETTINGS_NAME);

        if (!isset($settings['current_file']) || !file_exists($settings['current_file'])) {
            $settings['current_file'] = user_trailingslashit($this->get_data_path()) . date("Ymd") . $settings['extension'];
            if (!file_exists($settings['current_file'])) {
                WP_CLI::error("Brak pliku z danymi");
            }
        }

        $xml = new \XMLReader();
        $xml->open($settings['current_file']);

        while ($xml->read() && $xml->name != 'Apteka') {
            ;
        }

        $pharmacy_keys = $pharmacy_items = array();
        while ($xml->name == 'Apteka') {
            $element = new SimpleXMLElement($xml->readOuterXML());
            $pharmacy_name = str_replace( array("\"", "'"), "", $element->attributes()->nazwa->__toString() );
            $pharmact_type = $element->attributes()->rodzaj->__toString();
            $pharmacy_id = intval($element->attributes()->id);
            $pharmacy_status = $element->attributes()->status->__toString();
            $status = $pharmacy_status == "aktywna" ? 1 : 0;


            if ($status && !empty($pharmacy_name)) {
                $pharmacy_data = array_map('trim', array(
//                    'name' => ucfirst($pharmacy_name),
                    'internal_id' => $pharmacy_id,
                    'rodzaj' => $pharmact_type,
////                    'website' => mb_strtolower($element->attributes()->adresWWWApteki->__toString()),
////                    'phone' => $element->attributes()->numerTelefonu->__toString(),
////                    'postcode' => $element->Adres->attributes()->kodPocztowy->__toString(),
//                    'city' => mb_convert_case(mb_strtolower($element->Adres->attributes()->miejscowosc->__toString()), MB_CASE_TITLE, 'UTF-8'),
////                    'street_type' => $element->Adres->attributes()->typUlicy->__toString(),
//                    'street' => $element->Adres->attributes()->nazwaUlicy->__toString(),
//                    'house' => $element->Adres->attributes()->numerDomu->__toString(),
//                    'apartment' => $element->Adres->attributes()->numerLokalu->__toString(),
////                    'post' => mb_convert_case(mb_strtolower($element->Adres->attributes()->poczta->__toString()), MB_CASE_TITLE, 'UTF-8'),
////                    'state' => mb_convert_case(mb_strtolower($element->Adres->attributes()->wojewodztwo->__toString()), MB_CASE_TITLE, 'UTF-8'),
////                    'county' => mb_convert_case(mb_strtolower($element->Adres->attributes()->powiat->__toString()), MB_CASE_TITLE, 'UTF-8'),
////                    'community' => mb_convert_case(mb_strtolower($element->Adres->attributes()->gmina->__toString()), MB_CASE_TITLE, 'UTF-8'),
////                    'ulic_id' => $element->Adres->attributes()->ulic->__toString(),
////                    'simc_id' => $element->Adres->attributes()->simc->__toString(),
////                    'terc_id' => $element->Adres->attributes()->terc->__toString(),
////                    'whole_day' => 0,

                    'poniedzialek_file' => null,
//                    'poniedzialek_count' => null,
                    'poniedzialek_calc' => null,
                    'wtorek_file' => null,
//                    'wtorek_count' => null,
                    'wtorek_calc' => null,
                    'sroda_file' => null,
//                    'sroda_count' => null,
                    'sroda_calc' => null,
                    'czwartek_file' => null,
//                    'czwartek_count' => null,
                    'czwartek_calc' => null,
                    'piatek_file' => null,
//                    'piatek_count' => null,
                    'piatek_calc' => null,
                    'sobota_file' => null,
//                    'sobota_count' => null,
                    'sobota_calc' => null,
                    'niedziela_file' => null,
//                    'niedziela_count' => null,
                    'niedziela_calc' => null,
                ));

                $openings = $element->DniPracyApteki->DniPracy->children();
                if ( !empty($openings) ) {
                    $opening_data = array();
                    foreach ($openings as $day) {
                        $day_no = trim( sanitize_title( $day->attributes()->dzienTygodnia[0]->__toString() ) );
                        $open_hour = $day->attributes()->otwartaOd[0]->__toString();                    
                        $close_hour = $day->attributes()->otwartaDo[0]->__toString();

                        $pharmacy_data[ $day_no . "_file" ] = $open_hour . " --- " . $close_hour;
//                        $pharmacy_data[ $day_no . "_count" ] = strlen($open_hour) . " ### " . strlen($close_hour);

                        $open_hour = $this->get_converted_timestring($open_hour);
                        $close_hour = $this->get_converted_timestring($close_hour, true);

                        if( $open_hour && $close_hour) {
                            $fulltime = strtotime($close_hour) - strtotime($open_hour) == 0 || (strtotime($close_hour) - strtotime($open_hour)) / 60 / 60 == 24 ? 1 : 0;
                            if( ! $fulltime ) {
                                $pharmacy_data[ $day_no . "_calc" ] = $open_hour . " - " . $close_hour;
                            }
                        }
                    }
                }

                $pharmacy_items[] = $pharmacy_data;
                if ( empty($pharmacy_keys) ) {
                    $pharmacy_keys = array_keys($pharmacy_data);
                }
            }

            $xml->next('Apteka');
            unset($element);
        }

        $xml->close();
        
        $fp = fopen( PHARMA_CLI_DIR . '/data/debug_file.csv', 'w');
        fputcsv($fp, $pharmacy_keys, ";");
	foreach ($pharmacy_items as $fields) {
		fputcsv($fp, $fields,';');
	}
	fclose($fp);
        WP_CLI\Utils\format_items('table', $pharmacy_items, $pharmacy_keys);
        WP_CLI::success('Done.');
    }

    /**
     * 
     */
    public function handle_data() {
        $settings = get_option(PHARMA_CLI_SETTINGS_NAME);

        if (!isset($settings['current_file']) || !file_exists($settings['current_file'])) {
            $settings['current_file'] = user_trailingslashit($this->get_data_path()) . date("Ymd") . $settings['extension'];
            if (!file_exists($settings['current_file'])) {
                WP_CLI::error("Brak pliku z danymi");
            }
        }

        $state_taxonomy = get_terms(array(
            'taxonomy' => 'state',
            'hide_empty' => false,
            'fields' => 'id=>name'
        ));

        $city_taxonomy = get_terms(array(
            'taxonomy' => 'city',
            'hide_empty' => false,
            'fields' => 'id=>name'
        ));

        $xml = new \XMLReader();
        $xml->open($settings['current_file']);

        ignore_user_abort(true);
        wp_defer_term_counting(true);
        wp_defer_comment_counting(true);

        global $wpdb;
        $pharmacies = $wpdb->get_results("SELECT pharmacy_id, internal_id FROM {$wpdb->prefix}" . PHARMACY_DB_TABLE);
        $pharmacies = array_reduce($pharmacies, function ($result, $item) {
            $result[$item->internal_id] = $item->pharmacy_id;
            return $result;
        }, array());
        
        $wpdb->query('SET autocommit = 0;');
        $wpdb->query("TRUNCATE {$wpdb->prefix}" . PHARMACY_OPENINGS_DB_TABLE);

        while ($xml->read() && $xml->name != 'Apteka') {
            ;
        }
        
        $counter = 1;
        $pharmacy_keys = $pharmacy_items = array();
        while ($xml->name == 'Apteka') {
//            WP_CLI::line($counter);
            ++$counter;
            
            $element = new SimpleXMLElement($xml->readOuterXML());
            $owners = $element->Wlasciciele->children();
            $pharmacy_name = str_replace( array("\"", "'"), "", $element->attributes()->nazwa->__toString() );
            $pharmacy_id = intval($element->attributes()->id);
            $pharmacy_status = $element->attributes()->status->__toString();
            $status = $pharmacy_status == "aktywna" ? 1 : 0;
            $state = mb_convert_case(mb_strtolower($element->Adres->attributes()->wojewodztwo->__toString()), MB_CASE_TITLE, 'UTF-8');
            $city = mb_convert_case(mb_strtolower($element->Adres->attributes()->miejscowosc->__toString()), MB_CASE_TITLE, 'UTF-8');

            //WP_CLI::line($city . " / " . $state);

            foreach ($owners as $owner) {
                $pattern = array('mgr', 'farm.');
                if (!empty($owner->attributes()->imie) && !empty($owner->attributes()->nazwisko)) {
                    $pattern[] = $owner->attributes()->nazwisko->__toString() . " " . $owner->attributes()->imie->__toString();
                    $pattern[] = $owner->attributes()->imie->__toString() . " " . $owner->attributes()->nazwisko->__toString();
                    $pattern[] = $owner->attributes()->nazwisko->__toString();
                }

                $pharmacy_name = str_replace(
                        $pattern, '',
                        $pharmacy_name);
            }
			
			if( "-" == $pharmacy_name ) {
				$pharmacy_name = "";
			}

            if ($status && !empty($pharmacy_name)) {
				
                $state_id = array_search($state, $state_taxonomy);
                if (false === $state_id) {
                    $state_id = wp_insert_term($state, 'state');
                    if (!is_wp_error($state_id)) {
                        $state_id = $state_id['term_id'];
                        $state_taxonomy[$state_id] = $state;
                    } else {
                        if ($state_id->get_error_data('term_exists')) {
                            $state_taxonomy[$state_id->get_error_data('term_exists')] = $state;
                            $state_id = (int) $state_id->get_error_data('term_exists');
                        }
                    }
                }

                $city_id = array_search($city, $city_taxonomy);
                if (false === $city_id) {
                    $city_id = wp_insert_term($city, 'city');
                    if (!is_wp_error($city_id)) {
                        $city_id = $city_id['term_id'];
                        $city_taxonomy[$city_id] = $city;
                    } else {
                        if ($city_id->get_error_data('term_exists')) {
                            $city_taxonomy[$city_id->get_error_data('term_exists')] = $city;
                            $city_id = (int) $city_id->get_error_data('term_exists');
                        }
                    }
                }

                $pharmacy_data = array_map('trim', array(
                    'name' => $pharmacy_name,
                    'internal_id' => $pharmacy_id,
                    'status' => $status,
                    'website' => mb_strtolower($element->attributes()->adresWWWApteki->__toString()),
                    'phone' => $element->attributes()->numerTelefonu->__toString(),
                    'postcode' => $element->Adres->attributes()->kodPocztowy->__toString(),
                    'city' => mb_convert_case(mb_strtolower($element->Adres->attributes()->miejscowosc->__toString()), MB_CASE_TITLE, 'UTF-8'),
                    'street_type' => $element->Adres->attributes()->typUlicy->__toString(),
                    'street' => $element->Adres->attributes()->nazwaUlicy->__toString(),
                    'house' => $element->Adres->attributes()->numerDomu->__toString(),
                    'apartment' => $element->Adres->attributes()->numerLokalu->__toString(),
                    'post' => mb_convert_case(mb_strtolower($element->Adres->attributes()->poczta->__toString()), MB_CASE_TITLE, 'UTF-8'),
                    'state' => mb_convert_case(mb_strtolower($element->Adres->attributes()->wojewodztwo->__toString()), MB_CASE_TITLE, 'UTF-8'),
                    'county' => mb_convert_case(mb_strtolower($element->Adres->attributes()->powiat->__toString()), MB_CASE_TITLE, 'UTF-8'),
                    'community' => mb_convert_case(mb_strtolower($element->Adres->attributes()->gmina->__toString()), MB_CASE_TITLE, 'UTF-8'),
                    'ulic_id' => $element->Adres->attributes()->ulic->__toString(),
                    'simc_id' => $element->Adres->attributes()->simc->__toString(),
                    'terc_id' => $element->Adres->attributes()->terc->__toString(),
                    'whole_day' => 0,
                ));
                
                $pharmacy_name_suffix = "";
                
                if( trim(strtolower($pharmacy_name)) == "brak nazwy" ) {
                    $pharmacy_name = "Apteka";
                }
                
                if( trim(strtolower($pharmacy_name)) == "apteka" ) {
                    $pharmacy_name_suffix = sprintf(
                        "%s %s %s%s", 
                        $pharmacy_data['street_type'], 
                        $pharmacy_data['street'],
                        $pharmacy_data['house'],
                        !empty($pharmacy_data['apartment']) ? "/" . $pharmacy_data['apartment'] : ""
                    );
                    $pharmacy_name_suffix = " - " . trim($pharmacy_name_suffix);
                }

                $pharmacy = array(
                    'post_title' => $pharmacy_name . $pharmacy_name_suffix,
                    'post_name' => $pharmacy_id . "-" . sanitize_title($pharmacy_data['name']),
                    'post_type' => 'pharmacy',
                    'post_status' => 'publish'
                );

                if (isset($pharmacies[$pharmacy_id])) {
                    $pharmacy['ID'] = $pharmacies[$pharmacy_id];
                    $post_id = wp_update_post($pharmacy);
                } else {
                    $post_id = wp_insert_post($pharmacy);
                }

                if (!is_wp_error($post_id) && $post_id) {                    
                    $pharmacy_data['pharmacy_id'] = $post_id;
                    
                    wp_set_object_terms($post_id, array($state_id), 'state');
                    wp_set_object_terms($post_id, array($city_id), 'city');

                    if ($state_id && $city_id) {
                        $city_states = array($state_id);
                        $city_state_saved = get_field("state", "city_" . $city_id);
                        if ($city_state_saved && !in_array($state_id, $city_state_saved)) {
                            $city_states[] = $state_id;
                        }
                        update_field('state', $city_states, "city_" . $city_id);
                    }

                    if (isset($pharmacies[$pharmacy_id])) {
                        $pharmacy_data['updated_at'] = date("Y-m-d H:i:s");
                        $p_sql = $wpdb->update($wpdb->prefix . PHARMACY_DB_TABLE, $pharmacy_data, array("internal_id" => $pharmacy_id));
                    } else {
                        $p_sql = $wpdb->insert($wpdb->prefix . PHARMACY_DB_TABLE, $pharmacy_data);
                    }
                    if (!$p_sql) {
                        WP_CLI\Utils\format_items('table', array($pharmacy_data), array_keys($pharmacy_data));
                        WP_CLI::line($wpdb->print_error());
                    }

                    $openings = $element->DniPracyApteki->DniPracy->children();
                    if ( !empty($openings) ) {
                        $openings_temp = array();
                        foreach ($openings as $day) {
                            $day_number = $this->days_of_week[ trim( sanitize_title( $day->attributes()->dzienTygodnia[0]->__toString() ) ) ];
                            $open_hour = $day->attributes()->otwartaOd[0]->__toString();                    
                            $close_hour = $day->attributes()->otwartaDo[0]->__toString();
                            
                            $openings_temp[$day_number] = array(
                                'open' => null,
                                'close' => null
                            );

                            $open_hour = $this->get_converted_timestring($open_hour);
                            $close_hour = $this->get_converted_timestring($close_hour, true);

                            if( $open_hour && $close_hour ) {
                                $fulltime = strtotime($close_hour) - strtotime($open_hour) == 0 || (strtotime($close_hour) - strtotime($open_hour)) / 60 / 60 == 24 ? 1 : 0;
                                if( ! $fulltime ) {
                                    $openings_temp[$day_number]['close'] = $close_hour;
                                    $openings_temp[$day_number]['open'] = $open_hour;
                                }
                            }
                        }
                        
                        $openings_data = array_filter($openings_temp, function($el) {
                            return !empty($el['open']) && !empty($el['close']);
                        });
                        
                        if (empty($openings_data) ) {
                            // full time pharmacy
                            $pharmacy_full_time_data = array(
                                'updated_at' => date("Y-m-d H:i:s"),
                                'whole_day' => 1,
                            );
                            $wpdb->update($wpdb->prefix . PHARMACY_DB_TABLE, $pharmacy_full_time_data, array('internal_id' => $pharmacy_id));	
                        }
						
                        foreach( $openings_data as $day => $times) {
                            $pharmacy_openings_data = array(
                                'pharmacy_id' => $post_id,
                                'day' => $day,
                                'open' => $times['open'],
                                'close' => $times['close']
                            );
//                            if (isset($pharmacies[$pharmacy_id])) {
//                                $pharmacy_openings_data['updated_at'] = date("Y-m-d H:i:s");
//                                $wpdb->update($wpdb->prefix . PHARMACY_OPENINGS_DB_TABLE, $pharmacy_openings_data, array('pharmacy_id' => $pharmacies[$pharmacy_id]));
//                            } else {
                                $wpdb->insert($wpdb->prefix . PHARMACY_OPENINGS_DB_TABLE, $pharmacy_openings_data);
//                            }
                        }
                    }
                } else {
                    $pharmacy["internal_id"] = $pharmacy_id;
                    $pharmacy["original_name"] = strval($element->attributes()->nazwa);
                    var_dump($pharmacy);
//                    WP_CLI::line(wp_json_encode($post_id));
//                    WP_CLI\Utils\format_items( 'table', $pharmacy, array_keys($pharmacy) );
                }
            }

            $xml->next('Apteka');
            unset($element);
        }

        $xml->close();

//        WP_CLI\Utils\format_items( 'table', $pharmacy_items, $pharmacy_keys );
        $wpdb->query('COMMIT;');

        wp_defer_term_counting(false);
        wp_defer_comment_counting(false);

        WP_CLI::success('hello from get_data()! Path to data ' . PHARMA_CLI_DIR . '/data');
    }

    /*
     * 
     */

    public function download_data() {
        $settings = get_option(PHARMA_CLI_SETTINGS_NAME);

        /**
         * Bail in early stage
         */
        if (!isset($settings['url']) || !filter_var($settings['url'], FILTER_VALIDATE_URL)) {
            WP_CLI::error("Brak prawidłowego adresu URL pliku do ściągnięcia. Wejdź w panel administracyjny => Narzędzia => Pharma CLI i ustaw wymagane dane.");
        }

        WP_CLI::warning("Pobrano adres pliku. Trwa ściąganie...");

        /**
         * Get file to temporary directory
         */
        $temp_file = download_url($settings['url']);
        if (!is_wp_error($temp_file)) {
            WP_CLI::line("Plik został pobrany poprawnie do lokalizacji " . $temp_file);

            $data_path = $this->get_data_path();
            $target_file = user_trailingslashit($data_path) . date("Ymd") . $settings['extension'];
            $this->truncate_data_path();

            /**
             * Copy file from temporary directory
             */
            if (!copy($temp_file, $target_file)) {
                WP_CLI::error("Nie udało się skopiować pliku daty.");
            }

            WP_CLI::line("Plik został skopiowany do folderu docelowego.");

            /**
             * Update settings with current file path
             */
            $settings['current_file'] = $target_file;
            update_option(PHARMA_CLI_SETTINGS_NAME, $settings);

            /**
             * Remove temp file from memory
             */
            unlink($temp_file);
            WP_CLI::success("Usunięto i zakończono kopiowanie pliku z danymi.");
        } else {
            WP_CLI::warning($temp_file->get_error_message());
        }
    }

    /**
     * 
     */
    public function update_term_counter() {
        $terms = get_terms(array('taxonomy' => array('city', 'state'), 'hide_empty' => false));
        if (!is_wp_error($terms)) {
            foreach ($terms as $term) {
                wp_update_term_count_now(array($term->term_id), $term->taxonomy);
            }
        }
    }

    /**
     * Clean-up data directory
     */
    private function truncate_data_path() {
        $data_path = $this->get_data_path();
        foreach (glob("$data_path/*") as $file) {
            unlink($file);
        }

        WP_CLI::line("Opróżniono folder składujący pliki danych.");
    }

    /**
     * 
     * @return type
     */
    private function get_data_path() {
        return $this->data_path;
    }

    /**
     * 
     * @global type $wpdb
     * @param type $data
     */
    private function insert_pharmacy($data) {
        global $wpdb;
        $wpdb->insert($wpdb->prefix . PHARMACY_DB_TABLE, $data);
    }

    /**
     * 
     * @global type $wpdb
     * @param type $data
     */
    private function update_pharmacy($data) {
        global $wpdb;
        $wpdb->update($wpdb->prefix . PHARMACY_DB_TABLE, $data);
    }
    
    /**
     * 
     * @param type $input
     */
    private function get_converted_timestring($input, $look_from_end = false) {
        $pattern = $look_from_end ? '/([012]?[0-9]|2[0-3])([:\,\;\.][0-5][0-9])?$/m' : '/^([012]?[0-9]|2[0-3])([:\,\;\.][0-5][0-9])?/m';
        
        date_default_timezone_set('GMT');
        preg_match($pattern, $input, $input_set);
        if( !empty($input_set) ) {
            $input_temp = str_replace( array(",", ";", "."), ":", reset($input_set) );
            $input_temp = is_numeric($input_temp) && $input_temp < 10 ? str_pad($input_temp, 2, "0", STR_PAD_LEFT) : $input_temp;
            $input_temp = str_pad($input_temp, 4, "0", STR_PAD_RIGHT);
            return gmdate("H:i", strtotime($input_temp));
        }
        
        return false;
    }

}

WP_CLI::add_command('pharma-import', 'Import_Pharmacy_Data_CLI');
