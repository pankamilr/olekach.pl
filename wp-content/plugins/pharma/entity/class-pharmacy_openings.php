<?php
/**
 * Post API: Pharmacy_Openings class
 *
 * @package WordPress
 * @subpackage Post
 * @since 4.4.0
 */

/**
 * Core class used to implement the WP_Post object.
 *
 * @since 3.5.0
 *
 * @property string $page_template
 *
 * @property-read array  $ancestors
 * @property-read int    $post_category
 * @property-read string $tag_input
 */
final class Pharmacy_Openings {

	/**
	 * Post ID.
	 *
	 * @since 3.5.0
	 * @var int
	 */
	public $pharmacy_id;

	public $day;

	public $open;

	public $close;
        
	public $fulltime;
        
	/**
	 * Retrieve WP_Post instance.
	 *
	 * @since 3.5.0
	 *
	 * @global wpdb $wpdb WordPress database abstraction object.
	 *
	 * @param int $post_id Post ID.
	 * @return WP_Post|false Post object, false otherwise.
	 */
	public static function get_instance( $post_id ) {
		global $wpdb;

		$post_id = (int) $post_id;
		if ( ! $post_id ) {
			return false;
		}

		//$_post = wp_cache_get( $post_id, 'pharmacy_openings' );

		if ( ! $_post ) {
                    $_post = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}pharmacy_openings WHERE pharmacy_id = %d ORDER BY day LIMIT 6", $post_id ) );

                    if ( ! $_post ) {
                            return false;
                    }

                    $_post = sanitize_post( $_post, 'raw' );
                    wp_cache_add( $_post->pharmacy_id, $_post, 'pharmacy_openings' );
		} elseif ( empty( $_post->filter ) ) {
			$_post = sanitize_post( $_post, 'raw' );
		}

		return new Pharmacy_Openings( $_post );
	}

	/**
	 * Constructor.
	 *
	 * @since 3.5.0
	 *
	 * @param WP_Post|object $post Post object.
	 */
	public function __construct( $post ) {
		foreach ( get_object_vars( $post ) as $key => $value ) {
			$this->$key = $value;
		}
	}

	/**
	 * Isset-er.
	 *
	 * @since 3.5.0
	 *
	 * @param string $key Property to check if set.
	 * @return bool
	 */
	public function __isset( $key ) {
		
		return metadata_exists( 'pharmacy_openings', $this->pharmacy_id, $key );
	}

	/**
	 * Getter.
	 *
	 * @since 3.5.0
	 *
	 * @param string $key Key to get.
	 * @return mixed
	 */
	public function __get( $key ) {
                $value = get_post_meta( $this->pharmacy_id, $key, true );
		
		if ( $this->filter ) {
			$value = sanitize_post_field( $key, $value, $this->pharmacy_id, $this->filter );
		}

		return $value;
	}

	/**
	 * {@Missing Summary}
	 *
	 * @since 3.5.0
	 *
	 * @param string $filter Filter.
	 * @return array|bool|object|WP_Post
	 */
	public function filter( $filter ) {
		if ( $this->filter === $filter ) {
			return $this;
		}

		if ( 'raw' === $filter ) {
			return self::get_instance( $this->pharmacy_id );
		}

		return sanitize_post( $this, $filter );
	}

	/**
	 * Convert object to array.
	 *
	 * @since 3.5.0
	 *
	 * @return array Object as array.
	 */
	public function to_array() {
		$post = get_object_vars( $this );
		return $post;
	}
        
        public function get_today() {
            
        }
}
