<?php
/**
 * Post API: WP_Post class
 *
 * @package WordPress
 * @subpackage Post
 * @since 4.4.0
 */

/**
 * Core class used to implement the WP_Post object.
 *
 * @since 3.5.0
 *
 * @property string $page_template
 *
 * @property-read array  $ancestors
 * @property-read int    $post_category
 * @property-read string $tag_input
 */
final class Pharmacy {

	/**
	 * Post ID.
	 *
	 * @since 3.5.0
	 * @var int
	 */
	public $pharmacy_id;

	public $internal_id;

	public $status = 0;

	public $name;
        
	public $website;
        
        public $phone;

        public $postcode;
        
        public $city;
        
        public $street_type;
        
        public $street;
        
        public $house;
        
        public $apartment;
        
        public $post;
        
        public $state;
        
        public $county;
        
        public $community;
        
        public $latitude;
        
        public $longitude;
        
        public $today;
        
        public $openings;
		
		public $whole_day;
        
	/**
	 * Retrieve WP_Post instance.
	 *
	 * @since 3.5.0
	 *
	 * @global wpdb $wpdb WordPress database abstraction object.
	 *
	 * @param int $post_id Post ID.
	 * @return WP_Post|false Post object, false otherwise.
	 */
	public static function get_instance( $post_id ) {
		global $wpdb;

		$post_id = (int) $post_id;
		if ( ! $post_id ) {
			return false;
		}

		$_post = wp_cache_get( $post_id, 'pharmacy' );

		if ( ! $_post ) {
                    $_post = $wpdb->get_row( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}" . PHARMACY_DB_TABLE . " WHERE pharmacy_id = %d LIMIT 1", $post_id ) );

                    if ( ! $_post ) {
                            return false;
                    }

                    $_post = sanitize_post( $_post, 'raw' );
                    wp_cache_add( $_post->ID, $_post, 'pharmacy' );
		} elseif ( empty( $_post->filter ) ) {
			$_post = sanitize_post( $_post, 'raw' );
		}

		return new Pharmacy( $_post );
	}

	/**
	 * Constructor.
	 *
	 * @since 3.5.0
	 *
	 * @param WP_Post|object $post Post object.
	 */
	public function __construct( $post ) {
		foreach ( get_object_vars( $post ) as $key => $value ) {
			$this->$key = $value;
		}
	}

	/**
	 * Isset-er.
	 *
	 * @since 3.5.0
	 *
	 * @param string $key Property to check if set.
	 * @return bool
	 */
	public function __isset( $key ) {
		
		if ( 'openings' === $key ) {
			return true;
		}

		return metadata_exists( 'pharmacy', $this->pharmacy_id, $key );
	}

	/**
	 * Getter.
	 *
	 * @since 3.5.0
	 *
	 * @param string $key Key to get.
	 * @return mixed
	 */
	public function __get( $key ) {
		
		if ( 'openings' === $key ) {
                    return $this->get_openings();
		}
		
		if ( 'address' === $key ) {
                    return $this->get_address();
		}
		
		if ( 'address_html' === $key ) {
                    return $this->get_address(true);
		}
                
                if ( 'today' === $key ) {
                    return $this->today();
                }

                $value = get_post_meta( $this->pharmacy_id, $key, true );
		
		if ( $this->filter ) {
			$value = sanitize_post_field( $key, $value, $this->pharmacy_id, $this->filter );
		}

		return $value;
	}
        
        public function is_open() {
            $today_hours = $this->today();
            return $today_hours ? $today_hours->open > date("H:i") || ( $today_hours->open <= date("H:i") && $today_hours->close >= date("H:i") ) : false;
        }
        
        public function is_fulltime() {
            return (bool) $this->whole_day;
        }
        
        public function today() {
            return $this->get_openings( date('N') );
        }
        
        public function get_next_working_day() {
            $openings = $this->get_openings();
            $today = date("N");
            
            $past_days = array_slice($openings, 0, $today);
            $next_days = array_slice($openings, $today);
            $search_days = array_filter( array_merge($next_days, $past_days), function($el) { return !empty($el->open) && !empty($el->close); });

            $next_working_day = false;

            if ( !empty($search_days)) {
                $next_working_day = reset($search_days);
                $next_working_day->day = ( $today + 1 == $next_working_day->day_num ) ? "jutro" : $next_working_day->day;
            }
            return $next_working_day;
        }

	/**
	 * {@Missing Summary}
	 *
	 * @since 3.5.0
	 *
	 * @param string $filter Filter.
	 * @return array|bool|object|WP_Post
	 */
	public function filter( $filter ) {
		if ( $this->filter === $filter ) {
			return $this;
		}

		if ( 'raw' === $filter ) {
			return self::get_instance( $this->pharmacy_id );
		}

		return sanitize_post( $this, $filter );
	}

	/**
	 * Convert object to array.
	 *
	 * @since 3.5.0
	 *
	 * @return array Object as array.
	 */
	public function to_array() {
		$post = get_object_vars( $this );

		foreach ( array( 'ancestors', 'page_template', 'post_category', 'tags_input' ) as $key ) {
			if ( $this->__isset( $key ) ) {
				$post[ $key ] = $this->__get( $key );
			}
		}

		return $post;
	}
        
        public function get_address( $type = false ) {
            $address = array(
                sprintf( "%s %s %s%s", $this->street_type, $this->street, $this->house, !empty($this->apartment) ? "/" . $this->apartment : "" ),
                sprintf( "%s %s", $this->postcode, $this->city )
            );
            
            if( true === $type ) {
                $type = "html";
            }
            
            switch($type) {
                case "html":
                    return sprintf( '<p class="m-0">%s</p><p class="m-0">%s</p>', $address[0], $address[1] );
                case "array":
                    return $address;
                default:
                    return implode(", ", $address);
            }
        }
        
        public function get_openings( $day_to_show = false ) {            
            $_pharmacy_openings = wp_cache_get( $this->pharmacy_id, 'pharmacy_openings' );
            
            if( $_pharmacy_openings ) {
                if ( $day_to_show ) {
                    return isset( $_pharmacy_openings[ $day_to_show ] ) ? $_pharmacy_openings[ $day_to_show ] : false;
                }
                return $_pharmacy_openings;
            }
            
            global $wpdb;
            $_openings = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM {$wpdb->prefix}" . PHARMACY_OPENINGS_DB_TABLE . " WHERE pharmacy_id = %d ORDER BY day LIMIT 7", $this->pharmacy_id ) );
            $days = array();
            if ( $_openings ) {
                foreach( $_openings as $day ) {
                    $_post = wp_cache_get( $day->poid, 'pharmacy_opening_day' );
                    
                    if ( ! $_post ) {
                        $_post = sanitize_post( $_post, 'raw' );
                        wp_cache_add( $day->poid, $_post, 'pharmacy_opening_day' );
                    }
                    $days[$day->day]			= new \stdClass();
                    $days[$day->day]->open	= $day->open;
                    $days[$day->day]->close	= $day->close;
                    $days[$day->day]->all_day	= $day->fulltime;
                    $days[$day->day]->day_num	= $day->day;

                    $datetime = new \DateTime();
                    $datetime->setTimezone( new \DateTimeZone('Europe/Warsaw') );
                    $datetime->modify('Sunday this week')->modify( $day->day . " days" );
                    $days[$day->day]->day	= mysql2date('l', $datetime->format('Y-m-d'));
		}
                
                wp_cache_add( $this->pharmacy_id, $days, 'pharmacy_openings' );                
            }
            
            if ( $day_to_show ) {
                return isset( $days[ $day_to_show ] ) ? $days[ $day_to_show ] : false;
            }
            
            return $days;
        }
}
