<?php

/**
 * @author Kamil Ryczek <kamil.ryczek@artefakt.pl>
 * @copyright (c) 2019, Artefakt
 * @package olekach.pl
 */

?>
<div class="d-flex flex-column bg-white mb-4 shadow-sm">
	<h3 class="border-bottom p-3 pb-2 mb-0 d-flex justify-content-between align-items-center">Poczytaj na blogu<span class="btn btn-transparent btn-sm">więcej</span></h3>
	<ul class="nav nav-tabs" id="myTab" role="tablist">
		<?php echo $tab_navigation_items; ?>
	</ul>
	<div class="tab-content bg-white" id="myTabContent">
		<?php echo $tab_content_items; ?>
	</div>
</div>