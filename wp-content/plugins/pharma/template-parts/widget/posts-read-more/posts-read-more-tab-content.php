<?php

/**
 * @author Kamil Ryczek <kamil.ryczek@artefakt.pl>
 * @copyright (c) 2019, Artefakt
 * @package olekach.pl
 * 
 * @uses string $tab_pane_state
 * @uses WP_Term $category
 * @uses string $posts
 */
?>

<div class="tab-pane fade <?php echo $tab_pane_state; ?>" id="category_<?php echo $category->term_id; ?>_Tab" role="tabpanel" aria-labelledby="<?php echo $category->slug; ?>">
	<div class="pt-3 pb-1">
		<div class="container">
			<?php echo $posts; ?>
		</div>
	</div>
</div>