<?php

/**
 * @author Kamil Ryczek <kamil@piszekod.pl>
 * @copyright (c) 2019, O Lekach
 * @package olekach.pl
 */

?>

<div class="wrap">
	<h1>Pharma-CLI Settings</h1>
	<form method="POST" action="options.php">
	<?php
		settings_fields( 'pharma_cli_settings' );
		do_settings_sections( 'pharma-cli-settings' );
		submit_button();
	?>
	</form>
</div>

<style>
	select, input[type="url"], input[type="text"] {
		padding: 10px !important;
		height: auto !important;
	}
	input[type="url"], input[type="text"] {
		width: 50vw;
	}
</style>