<?php

/**
 * @author Kamil Ryczek <kamil.ryczek@artefakt.pl>
 * @copyright (c) 2019, Artefakt
 * @package olekach.pl
 */

if( !class_exists('Pharma_Cities_In_State_Widget') ) {
	class Pharma_Cities_In_State_Widget extends WP_Widget {
		
		/**
		 * 
		 */
		public function __construct() {
			parent::__construct(
				'pharma_cities_in_state_widget',
				'Pharma - Miasta w województwie',
				array( 'description' => "Widget wyświetlający listę miast w danym województwie" )
			);
		}
		
		/**
		 * 
		 * @param type $instance
		 */
		public function form( $instance ) {
			$title = $instance && isset( $instance['title'] ) ? $instance['title'] : "";
			$limit = $instance && isset( $instance['limit'] ) ? $instance['limit'] : 10;
			$link_archive = $instance && isset( $instance['link_archive'] ) ? (int) $instance['link_archive'] : 0;
			
			?>
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>">Tytuł</label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'limit' ); ?>">Liczba elementów</label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'limit' ); ?>" name="<?php echo $this->get_field_name( 'limit' ); ?>" type="number" value="<?php echo esc_attr( $limit ); ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'link_archive' ); ?>">Pokaż link do listy aptek</label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'link_archive' ); ?>" name="<?php echo $this->get_field_name( 'link_archive' ); ?>" type="checkbox" value="1" <?php checked($link_archive, "1"); ?> />
			</p>
		<?php
		}
		
		/**
		 * 
		 * @param type $new_instance
		 * @param type $old_instance
		 * @return type
		 */
		public function update( $new_instance, $old_instance ) {
			$instance['title'] = strip_tags( $new_instance['title'] );
			$instance['limit'] = strip_tags( $new_instance['limit'] );
			$instance['link_archive'] = strip_tags( $new_instance['link_archive'] );
			return $instance;
		}
		
		/**
		 * 
		 * @param type $args
		 * @param type $instance
		 */
		public function widget( $args, $instance ) {
			$count = get_option( 'akismet_spam_count' );

			if ( ! isset( $instance['title'] ) ) {
				$instance['title'] = __( 'Spam Blocked' , 'akismet' );
			}
                        
                        if( is_tax( 'state' ) ) {
                            if( $cities = get_terms( array( 'taxonomy' => 'city', 'meta_query' => array( array( 'key' => 'state', 'value' => 'i:'.get_queried_object()->term_id.';', 'compare' => 'LIKE' )) ) ) ) {
                        
                            echo $args['before_widget'];
                            if ( ! empty( $instance['title'] ) ) {
                                    echo $args['before_title'];
                                    echo sprintf(esc_html( $instance['title'] ), single_term_title('', false) );
                                    echo $args['after_title'];
                            } 
                            ?>
                             <div class="p-3 cities-in-state-widget"><ul class="list-inline row roll-up">
                                <?php foreach( $cities as $city ) { ?> 
                                <li class="list-inline-item m-0 col col-md-6"><a href="<?php echo get_term_link($city); ?>" class="text-truncate d-block">Apteka <?php echo $city->name; ?></a></li>
                                <?php } ?>
                                 </ul>
                                 <span class="d-block text-center cities-roll c-pointer roll-down">Pokaż więcej</span>
                             </div>
                        <?php }
                            echo $args['after_widget'];
                        }
		}
	}
}
