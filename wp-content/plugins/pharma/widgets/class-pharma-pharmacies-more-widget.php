<?php

/**
 * @author Kamil Ryczek <kamil.ryczek@artefakt.pl>
 * @copyright (c) 2019, Artefakt
 * @package olekach.pl
 */

if( !class_exists('Pharma_Pharmacies_More_Widget') ) {
	class Pharma_Pharmacies_More_Widget extends WP_Widget {
		
		/**
		 * 
		 */
		public function __construct() {
			parent::__construct(
				'pharma_pharmacies_more_widget',
				'Pharma - Apteki w innych miastach',
				array( 'description' => "Widget wyświetlający listę miast z aptekami" )
			);
		}
		
		/**
		 * 
		 * @param type $instance
		 */
		public function form( $instance ) {
			$title = $instance && isset( $instance['title'] ) ? $instance['title'] : "";
			$limit = $instance && isset( $instance['limit'] ) ? $instance['limit'] : 10;
			$link_archive = $instance && isset( $instance['link_archive'] ) ? (int) $instance['link_archive'] : 0;
			
			?>
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>">Tytuł</label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'limit' ); ?>">Liczba elementów</label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'limit' ); ?>" name="<?php echo $this->get_field_name( 'limit' ); ?>" type="number" value="<?php echo esc_attr( $limit ); ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'link_archive' ); ?>">Pokaż link do listy aptek</label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'link_archive' ); ?>" name="<?php echo $this->get_field_name( 'link_archive' ); ?>" type="checkbox" value="1" <?php checked($link_archive, "1"); ?> />
			</p>
		<?php
		}
		
		/**
		 * 
		 * @param type $new_instance
		 * @param type $old_instance
		 * @return type
		 */
		public function update( $new_instance, $old_instance ) {
			$instance['title'] = strip_tags( $new_instance['title'] );
			$instance['limit'] = strip_tags( $new_instance['limit'] );
			$instance['link_archive'] = strip_tags( $new_instance['link_archive'] );
			return $instance;
		}
		
		/**
		 * 
		 * @param type $args
		 * @param type $instance
		 */
		public function widget( $args, $instance ) {
			$count = get_option( 'akismet_spam_count' );

			if ( ! isset( $instance['title'] ) ) {
				$instance['title'] = __( 'Spam Blocked' , 'akismet' );
			}

			echo $args['before_widget'];
			if ( ! empty( $instance['title'] ) ) {
				echo $args['before_title'];
				echo esc_html( $instance['title'] );
                                echo '<a href="' . get_post_type_archive_link('pharmacy') . '" class="btn btn-transparent btn-sm text-uppercase">Apteki w innych miastach</a>';
				echo $args['after_title'];
			}

                        $city_terms = get_terms( array( 'taxonomy' => 'city', 'orderby' => 'count', 'order' => 'DESC', 'hide_empty' => false, 'number' => 10 ) ); 
                        if( $city_terms ) { ?>
                        <div class="p-3"><ul class="list-inline row">
                        <?php foreach( $city_terms as $term ) { ?>
                            <li class="d-flex justify-content-start align-items-center list-inline-item m-0 col-12 col-sm-6 col-lg-12 col-xl-6"><span class="badge bg-light-red badge-pill align-self-center mr-3"><?php echo $term->count; ?></span> <a href="<?php echo get_term_link($term); ?>">Apteka <?php echo $term->name; ?></a></li>
                        <?php } ?>
                            </ul></div>
                        <?php }

			echo $args['after_widget'];
		}
	}
}
