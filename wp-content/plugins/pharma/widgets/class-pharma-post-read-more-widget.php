<?php
/**
 * @author Kamil Ryczek <kamil.ryczek@artefakt.pl>
 * @copyright (c) 2019, Artefakt
 * @package olekach.pl
 */
if (!class_exists('Pharma_Posts_Read_More_Widget')) {

    class Pharma_Posts_Read_More_Widget extends WP_Widget {

        /**
         * 
         */
        public function __construct() {
            parent::__construct(
                    'pharma_posts_read_more_widget',
                    'Pharma - Poczytaj również na blogu',
                    array('description' => "Widget wyświetlający posty blogowe z kategorii")
            );
        }

        /**
         * 
         * @param type $instance
         */
        public function form($instance) {
            if ($instance && isset($instance['title'])) {
                $title = $instance['title'];
            } else {
                $title = __('Poczytaj również na blogu', 'pharma');
            }
            ?>
            <p>
                <label for="<?php echo $this->get_field_id('title'); ?>">Tytuł</label>
                <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
            </p>
            <?php
        }

        /**
         * 
         * @param type $new_instance
         * @param type $old_instance
         * @return type
         */
        public function update($new_instance, $old_instance) {
            $instance['title'] = strip_tags($new_instance['title']);
            return $instance;
        }

        /**
         * 
         * @param type $args
         * @param type $instance
         */
        public function widget($args, $instance) {
            $count = get_option('akismet_spam_count');

            if (!isset($instance['title'])) {
                $instance['title'] = __('Spam Blocked', 'akismet');
            }
			
            $categories = get_categories();
            $tab_navigation_items = $tab_content_items = "";
            $active_index = 0;
            foreach ($categories as $idx => $category) {

                global $post;
                $posts_from_categories = get_posts(array('category' => array($category->term_id), 'posts_per_page' => 50, 'exclude' => array($post->ID)));
                
                if(!empty($posts_from_categories) ) {
                    $tab_active_class = $active_index === 0 ? "active show" : "";
                    $tab_navigation_items .= sprintf('<li class="nav-item">'
                            . '<a class="nav-link p-3 mb-0 border-top-0 border-left-0 border-right %1$s" id="%2$s-navtab-item" data-toggle="tab" href="#category_%3$d_Tab" role="tab" aria-controls="%2$s" aria-selected="%4$s">%5$s</a>'
                            . '</li>', $tab_active_class, $category->slug, $category->term_id, empty($tab_active_class) ? "false" : "true", $category->name);
                    $posts_loop = "";
                    $posts_from_categories_count = count($posts_from_categories);
                    foreach ($posts_from_categories as $jdx => $post_from_category) {
                        if ($jdx === 0) {
                            $posts_loop .= sprintf('<div class="row">
                                                    <div class="col-12 col-md-5">
                                                            <div class="bg-light-yellow">
                                                                    <a href="%1$s">%4$s</a>
                                                            </div>							
                                                    </div>
                                                    <div class="col-12 col-md-7">
                                                            <h4 class="mb-1 mt-0 h4 font-weight-normal mb-1"><a href="%1$s" title="%2$s">%2$s</a></h4>
                                                            <div class="d-flex justify-content-between align-items-center">
                                                                    <small class="d-flex align-items-center"><i class="material-icons mr-3">access_time</i>%3$s</small>
                                                                    <a href="%1$s" class="btn bg-dark-green text-white mx-0 p-2">Czytaj dalej</a>
                                                            </div>
                                                    </div>
                                            </div>', get_permalink($post_from_category->ID), $post_from_category->post_title, get_the_date('m.d.Y', $post_from_category->ID), get_the_post_thumbnail($post_from_category->ID, 'post_thumbnail', array('class' => 'img-fluid')));
                            
                            if($posts_from_categories_count > 1) {
                                $posts_loop .= '<div class="row">
                                                    <div class="col">
                                                    <ul class="list-group-flush ml-0 pl-0 mt-3 mb-0 border-top">';
                            }
                        } else {
                            $posts_loop .= sprintf('<li class="list-group-item px-0 d-flex justify-content-between align-items-end %4$s">
                                                                            <a href="%1$s" class="text-truncate d-inline-block">%2$s</a>
                                                                            <small class="d-flex align-items-center text-muted text-small">%3$s</small>
                                                                    </li>', get_permalink($post_from_category->ID), $post_from_category->post_title, get_the_date('m.d.Y', $post_from_category->ID), ($jdx == $posts_from_categories_count - 1) ? "pb-0" : "");
                        }

                        if ($posts_from_categories_count > 1 && $jdx == $posts_from_categories_count - 1) {
                            $posts_loop .= '</ul></div></div>';
                        }
                    }

                    $tab_content_items .= sprintf('<div class="tab-pane fade %1$s" id="category_%2$d_Tab" role="tabpanel" aria-labelledby="%3$s">'
                            . '<div class="py-3">'
                            . '<div class="container">%4$s</div>'
                            . '</div>'
                            . '</div>', $tab_active_class, $category->term_id, $category->slug, $posts_loop);
                    
                    ++$active_index;
                }
            }
					
			if($active_index > 0) {
				echo str_replace("p-3", "", $args['before_widget']);
				if (!empty($instance['title'])) {
					echo $args['before_title'];
					echo esc_html($instance['title']);
					echo '<span class="btn btn-transparent btn-sm">więcej</span>';
					echo $args['after_title'];
				}
            ?>

            
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <?php echo $tab_navigation_items; ?>
                </ul>
                <div class="tab-content bg-white" id="myTabContent">
                    <?php echo $tab_content_items; ?>
                </div>
            <?php
            echo $args['after_widget'];
			}
        }

    }

}
