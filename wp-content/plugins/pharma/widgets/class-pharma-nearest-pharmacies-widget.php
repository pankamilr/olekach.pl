<?php
/**
 * @author Kamil Ryczek <kamil.ryczek@artefakt.pl>
 * @copyright (c) 2019, Artefakt
 * @package olekach.pl
 */
if (!class_exists('Pharma_Nearest_Pharmacies_Widget')) {

	class Pharma_Nearest_Pharmacies_Widget extends WP_Widget {

		/**
		 * 
		 */
		public function __construct()
		{
			parent::__construct(
					'pharma_nearest_pharmacies_widget', 'Pharma - Najbliższe apteki', array('description' => "Widget wyświetlający apteki najbliższe do aktualnie wyświetlanej")
			);
		}

		/**
		 * 
		 * @param type $instance
		 */
		public function form($instance)
		{
			$distance = $limit = 1;
			$title = "";
			
			if ($instance && isset($instance['title'])) {
				$title = $instance['title'];
			}
			
			if ($instance && isset($instance['distance'])) {
				$distance = $instance['distance'];
			}
			
			if ($instance && isset($instance['limit'])) {
				$limit = $instance['limit'];
			}
			?>
			<p>
				<label for="<?php echo $this->get_field_id('title'); ?>">Tytuł</label>
				<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('distance'); ?>">W jakim promieniu szukać (w km)?</label>
				<input class="widefat" id="<?php echo $this->get_field_id('distance'); ?>" name="<?php echo $this->get_field_name('distance'); ?>" type="number" value="<?php echo esc_attr($distance); ?>" />
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('limit'); ?>">Ile wyników pokazać?</label>
				<input class="widefat" id="<?php echo $this->get_field_id('limit'); ?>" name="<?php echo $this->get_field_name('limit'); ?>" type="number" value="<?php echo esc_attr($limit); ?>" />
			</p>
			<?php
		}

		/**
		 * 
		 * @param type $new_instance
		 * @param type $old_instance
		 * @return type
		 */
		public function update($new_instance, $old_instance)
		{
			$instance['title'] = strip_tags($new_instance['title']);
			$instance['distance'] = strip_tags($new_instance['distance']);
			$instance['limit'] = strip_tags($new_instance['limit']);
			return $instance;
		}

		/**
		 * 
		 * @param type $args
		 * @param type $instance
		 */
		public function widget($args, $instance)
		{
			global $post;

			if (!$post instanceof \WP_Post || !is_singular('pharmacy')) {
				return;
			}
			
                        $lat = $post->data->latitude;
                        $lng = $post->data->longitude;
                        
                        if( isset($_COOKIE['currentUserLatLng']) ) {
                            $user_location = urldecode($_COOKIE['currentUserLatLng']);
                            list($lat, $lng) = explode(";", $user_location);
                        }
                        
			if( $pharmacies = get_nearest_pharmacies_by_lat_lon($lat, $lng, array($post->ID), $instance['distance'], $instance['limit'] ) ) {
				set_query_var('current_nearest_pharmacies', $pharmacies);
				set_query_var('current_nearest_pharmacies_distance', $instance['distance']);
				
				$pharmacy_city = get_the_terms($post, 'city');
				echo $args['before_widget'];

				echo $args['before_title'];
				if (!empty($instance['title'])) {
					echo esc_html($instance['title']);
				}
				if ($pharmacy_city = reset($pharmacy_city)) {
					echo sprintf('<a href="%s" class="btn btn-transparent btn-sm">pozostałe w %s</a>', get_term_link($pharmacy_city), $pharmacy_city->name);
				}
				echo $args['after_title'];

				echo '<ul class="list-group-flush ml-0 pl-0 mb-0">';
				foreach ($pharmacies as $pharmacy) {
                                    $distance = round($pharmacy->distance * 1000);
                                    $distance = $distance > 1000 ? round($distance/1000, 1) . " km" : $distance . " m";
                                    
					if ((int) $pharmacy->pharmacy_id !== $post->ID) {
						echo sprintf('<li class="list-group-item px-3 d-flex flex-column justify-content-between">
														<div class="d-flex flex-column flex-md-row align-items-start align-items-md-center">
															<div class="distance d-flex align-items-center mr-2 my-2 my-md-0 badge bg-light-red order-1 order-md-0">
																<i class="material-icons">near_me</i>
																<span class="ml-1">%1$s</span>
                                                                                                                                <span class="ml-1 mr-2 d-md-none">od ul. Katoickiej 1</span>
															</div>
															<h3 class="h5 m-0 p-0 font-weight-light"><a href="%7$s">%2$s</a></h3>
														</div>
														<div class="address mt-2 d-flex justify-content-between">
															%3$s %4$s %5$s, %6$s
															<span class="open">dziś do 19:00</span>
														</div>
													</li>', $distance, get_the_title( $pharmacy->pharmacy_id ), $pharmacy->street_type, $pharmacy->street, $pharmacy->house, $pharmacy->city, get_permalink($pharmacy->pharmacy_id));
					}
				}
				echo '</ul>';
				echo $args['after_widget'];
			}
		}

	}

}
