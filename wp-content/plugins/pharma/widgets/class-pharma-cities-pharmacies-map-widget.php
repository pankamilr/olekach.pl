<?php
/**
 * @author Kamil Ryczek <kamil.ryczek@artefakt.pl>
 * @copyright (c) 2019, Artefakt
 * @package olekach.pl
 */
if (!class_exists('Pharma_Cities_Pharmacies_Map_Widget')) {

    class Pharma_Cities_Pharmacies_Map_Widget extends WP_Widget {

        /**
         * 
         */
        public function __construct() {
            parent::__construct(
                    'pharma_cities_pharmacies_map_widget',
                    'Pharma - Mapa aptek w mieście',
                    array('description' => "Widget wyświetlający mapę z pinami aptek naniesionymi na obszar")
            );
        }

        /**
         * 
         * @param type $instance
         */
        public function form($instance) {
            $title = $instance && isset($instance['title']) ? $instance['title'] : "";
            $limit = $instance && isset($instance['limit']) ? $instance['limit'] : 10;
            $link_archive = $instance && isset($instance['link_archive']) ? (int) $instance['link_archive'] : 0;
            ?>
            <p>
                <label for="<?php echo $this->get_field_id('title'); ?>">Tytuł</label>
                <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('limit'); ?>">Liczba elementów</label>
                <input class="widefat" id="<?php echo $this->get_field_id('limit'); ?>" name="<?php echo $this->get_field_name('limit'); ?>" type="number" value="<?php echo esc_attr($limit); ?>" />
            </p>
            <p>
                <label for="<?php echo $this->get_field_id('link_archive'); ?>">Pokaż link do listy aptek</label>
                <input class="widefat" id="<?php echo $this->get_field_id('link_archive'); ?>" name="<?php echo $this->get_field_name('link_archive'); ?>" type="checkbox" value="1" <?php checked($link_archive, "1"); ?> />
            </p>
            <?php
        }

        /**
         * 
         * @param type $new_instance
         * @param type $old_instance
         * @return type
         */
        public function update($new_instance, $old_instance) {
            $instance['title'] = strip_tags($new_instance['title']);
            $instance['limit'] = strip_tags($new_instance['limit']);
            $instance['link_archive'] = strip_tags($new_instance['link_archive']);
            return $instance;
        }

        /**
         * 
         * @param type $args
         * @param type $instance
         */
        public function widget($args, $instance) {

            if (is_tax('city')) {
                if ($posts = get_posts(array(
                    'post_type' => 'pharmacy',
                    'posts_per_page' => -1,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'city',
                            'terms' => array(get_queried_object_id())
                        )
                    )
                        ))
                ) {

                    echo $args['before_widget'];
                    if (!empty($instance['title'])) {
                        echo $args['before_title'];
                        echo sprintf(esc_html($instance['title']), single_term_title('', false));
                        echo $args['after_title'];
                    }
                    ?>
                    <div id="map" style="height:50vh;max-height:500px;"></div>
                    <script type="text/javascript">
                        (function ($) {

                            var ajaxCall = jQuery.ajax({
                                dataType: 'json',
                                url: 'https://nominatim.openstreetmap.org/search?q=' + pharmacy.current_term.name + '&format=json&polygon=0&addressdetails=1'
                            });

                            ajaxCall.done(function (data) {
                                var addressPoints = [<?php
                    foreach ($posts as $post) {
                        setup_postdata($post);
                        if ($post->data->latitude && $post->data->longitude) {
                            ?>
                                            [<?php echo $post->data->latitude; ?>, <?php echo $post->data->longitude; ?>, "<?php echo htmlentities(get_the_title($post)); ?><br /><a href=\"<?php echo get_permalink($post); ?>\">Przejdź</a>"],
                        <?php }
                    } ?>],
                                        mapurl = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',
                                        grayscale = L.tileLayer(mapurl, {id: 'mapbox.light'}),
                                        streets = L.tileLayer(mapurl, {id: 'mapbox.streets'}),
                                        latlng = L.latLng(data[0].lat, data[0].lon),
                                        map = L.map('map', {
                                            layers: [grayscale],
                                            center: latlng,
                                            zoom: 15
                                        }),
                                        baseLayers = {
                                            "Monochromatyczna": grayscale,
                                            "Kolorowa": streets
                                        },
                                        markers = L.markerClusterGroup();
                                L.control.layers(baseLayers).addTo(map);
                                
                                for (var i = 0; i < addressPoints.length; i++) {
                                    var a = addressPoints[i];
                                    var title = a[2];
                                    var marker = L.marker(new L.LatLng(a[0], a[1]), {title: title});
                                    marker.bindPopup(title);
                                    markers.addLayer(marker);
                                }

                                map.addLayer(markers);
                            });
                        })(jQuery);
                    </script>
                    <?php
                }
                echo $args['after_widget'];
            }
        }

    }

}
