<?php
/**
 * @author Kamil Ryczek <kamil.ryczek@artefakt.pl>
 * @copyright (c) 2019, Artefakt
 * @package olekach.pl
 */
if (!class_exists('Pharma_Synonymous_Text_Widget')) {

    class Pharma_Synonymous_Text_Widget extends WP_Widget {

        /**
         * 
         */
        public function __construct() {
            parent::__construct(
                    'pharma_synonymous_text_widget',
                    'Pharma - Teksty synonimiczne',
                    array('description' => "Widget wyświetlający tekst synonimiczny zapisany dla danej kategorii")
            );
        }

        /**
         * 
         * @param type $instance
         */
        public function form($instance) {
            $title = $instance && isset($instance['title']) ? $instance['title'] : "";
            ?>
            <p>
                <label for="<?php echo $this->get_field_id('title'); ?>">Tytuł</label>
                <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
            </p>
            <?php
        }

        /**
         * 
         * @param type $new_instance
         * @param type $old_instance
         * @return type
         */
        public function update($new_instance, $old_instance) {
            $instance['title'] = strip_tags($new_instance['title']);
            return $instance;
        }

        /**
         * 
         * @param type $args
         * @param type $instance
         */
        public function widget($args, $instance) {

            if (!isset($instance['title'])) {
                $instance['title'] = "";
            }

            if (is_tax('state') || is_tax('city')) {
                $term = get_queried_object();
                $term_synonymous_text = get_field('synonymous_text', $term->taxonomy . '_' . $term->term_id);

                if (empty($term_synonymous_text)) {
                    $synonymous_repeater = get_field('synonym_repeater', 'options');
                    $random_field = $synonymous_repeater[mt_rand(0, count($synonymous_repeater) - 1)];

                    if ($random_field) {
                        $term_synonymous_text = \Spintax::process($random_field['synonymous_text']);
                        $term_synonymous_text = str_replace(array("%miasto%"), array($term->name), $term_synonymous_text);
                        update_field('synonymous_text', $term_synonymous_text, $term->taxonomy . '_' . $term->term_id);
                    }
                }

                if ($term_synonymous_text) {
                    echo $args['before_widget'];
                    if (!empty($instance['title'])) {
                        echo $args['before_title'];
                        echo esc_html($instance['title']);
                        echo $args['after_title'];
                    }
                    ?>
                    <div class="p-4 synonymous-text-widget">
                    <?php echo $term_synonymous_text; ?>
                    </div>
                        <?php
                        echo $args['after_widget'];
                    }
                }
            }

        }

    }
