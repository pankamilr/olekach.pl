<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
  <head>
    <title>O lekach</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
      #img-logo { margin-top: 5% !important; }
      #maintaince-main-container { margin-top: 40px; }
      #maintaince-main-container #header h1.sitename { margin-bottom: 30px; }
    </style>
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col-xs-15 text-center">
        </div>
      </div>
    </div>
    <div id="maintaince-main-container" class="container">
      <div id="header">

        <!-- Header -->
        <div class="row">
          <div class="col-xs-15 col-sm-13 col-md-13 col-xs-offset-0 col-sm-offset-1 col-md-offset-1 slogan-col">
            <div id="user-nav-menu" class="collapse"></div>
            <div class="clear clearfix"></div>

            <h1 class="text-center text-uppercase sitename">O lekach</h1>
          </div>
        </div>
      </div>

      <div id="content"></div>
    </div>
  </body>
</html>
