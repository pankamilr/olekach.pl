<?php
/**
 * @author Kamil Ryczek <kamil.ryczek@artefakt.pl>
 * @copyright (c) 2019, Artefakt
 * @package olekach.pl
 */
get_header();
?>

<div class="container">
    <div class="row flex-row-reverse ">
        <div class="col-12 col-md-4">
            <?php get_sidebar('post'); ?>
        </div>

        <div class="col-12 col-md-8">
            <div class="pr-5">
                <?php
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                        ?>
                        <div class="title bg-white px-5 pt-5 pb-4">
                            <h1><?php the_title(); ?></h1>

                        </div>
                        <div class="body bg-white text-lg p-5" itemprop="articleBody">
                            <?php the_content(); ?>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>	
</div>

<?php
get_footer();
