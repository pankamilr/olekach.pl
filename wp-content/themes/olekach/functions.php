<?php

/**
 * @author Kamil Ryczek <kamil@piszekod.pl>
 * @copyright (c) 2019, O Lekach
 * @package olekach.pl
 */


add_action('after_setup_theme', 'olekach_theme_setup');
function olekach_theme_setup()
{
    add_theme_support('post-thumbnails');
    add_theme_support('title-tag');

    add_image_size('heading-thumb', 567, 453, true); // (cropped)

    /*
     * Enable support for Menu
     * This theme uses wp_nav_menu() in two locations.
     * 
     * See: https://codex.wordpress.org/Function_Reference/register_nav_menus
     * See: https://developer.wordpress.org/reference/functions/wp_nav_menu/
     */
    register_nav_menus(array(
        'primary' => 'Top Menu',
        'footer_1' => 'Stopka 1',
        'footer_2' => 'Stopka 2',
        'footer_3' => 'Stopka 3',
    ));
}

/**
 * Install required child-scripts callback
 */
function olekach_load_scripts()
{
    wp_enqueue_style('roboto-font-file', get_stylesheet_directory_uri() . "/assets/font/roboto/Roboto-Bold.woff2", array(), null);
    wp_register_style('bootstrap', get_stylesheet_directory_uri() . '/assets/css/bootstrap.min.css', false, rand(), 'all');
    wp_register_style('fonts', 'https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700|Material+Icons&display=swap&subset=latin-ext', false, rand(), 'all');
    wp_register_style('mdbootstrap', get_stylesheet_directory_uri() . '/assets/css/mdb.lite.min.css', false, rand(), 'all');
    wp_register_style('leaflet', get_stylesheet_directory_uri() . '/assets/css/leaflet.css', false, rand(), 'all');
    wp_register_style('leaflet_cluster', get_stylesheet_directory_uri() . '/assets/css/MarkerCluster.css', false, rand(), 'all');
    wp_register_style('leaflet_cluster_dflt', get_stylesheet_directory_uri() . '/assets/css/MarkerCluster.Default.css', false, rand(), 'all');
    wp_register_style('toastr', get_stylesheet_directory_uri() . '/assets/css/toastr.min.css', false, rand(), 'all');
    wp_register_style('main-style', get_stylesheet_directory_uri() . '/assets/css/olekach.css', array('bootstrap', 'leaflet'), rand(), 'all');

    wp_enqueue_style('bootstrap');
    wp_enqueue_style('fonts');
    wp_enqueue_style('mdbootstrap');
    wp_enqueue_style('toastr');
    wp_enqueue_style('main-style');


    wp_deregister_script('jquery');

    wp_register_script('jquery', get_stylesheet_directory_uri() . '/assets/js/jquery-3.4.1.min.js', array(), rand());
    wp_register_script('popper', get_stylesheet_directory_uri() . '/assets/js/popper.js', array(), rand(), true);
//	wp_register_script( 'material',		get_stylesheet_directory_uri().'/assets/js/bootstrap-material-design.js', array(), rand() );
    wp_register_script('bootstrap', get_stylesheet_directory_uri() . '/assets/js/bootstrap.bundle.min.js', array(), rand(), true);
    wp_register_script('leaflet', get_stylesheet_directory_uri() . '/assets/js/leaflet.js', array('jquery'), rand());
    wp_register_script('leaflet_cluster', get_stylesheet_directory_uri() . '/assets/js/leaflet.markercluster.js', array('leaflet'), rand());
    wp_register_script('js-cookie', get_stylesheet_directory_uri() . '/assets/js/js.cookie.min.js', array('leaflet'), rand());
    wp_register_script('toastr', get_stylesheet_directory_uri() . '/assets/js/toastr.min.js', array('leaflet'), rand());
    wp_register_script('app', get_stylesheet_directory_uri() . '/assets/js/app.js', array('jquery', 'leaflet', 'js-cookie', 'wp-util', 'toastr'), rand());

    wp_enqueue_script('jquery');
    wp_enqueue_script('bootstrap');
    wp_enqueue_script('popper');
    wp_enqueue_script('material');
    wp_enqueue_script('js-cookie');
    wp_enqueue_script('app');

    $localize_script_data = array(
        'current_term' => get_queried_object(),
        'map_url' => 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',
        'ajaxurl' => admin_url('admin-ajax.php')
    );

    if (is_singular('pharmacy') || is_tax("city")) {
        if (is_singular('pharmacy')) {
            global $post;
            $localize_script_data['single_pharmacy'] = ['map' => []];

            $localize_script_data['single_pharmacy']['map']['center'] = [$post->data->latitude, $post->data->longitude];
            $localize_script_data['single_pharmacy']['map']['icon_path'] = get_template_directory_uri();
            $localize_script_data['single_pharmacy']['map']['title'] = htmlentities($post->post_title);
            $localize_script_data['single_pharmacy']['map']['siblings'] = [];

            if( $current_nearest_pharmacies = get_nearest_pharmacies() ) {
                foreach($current_nearest_pharmacies as $nearest_pharmacy) {
                    $localize_script_data['single_pharmacy']['map']['siblings'][] = [
                        'lat' => $nearest_pharmacy->latitude,
                        'long' => $nearest_pharmacy->longitude,
                        'title' => htmlentities( get_the_title( $nearest_pharmacy->pharmacy_id ) ),
                        'link' => sprintf("<br /><a href=\"%s\">Przejdź</a>", get_permalink($nearest_pharmacy->pharmacy_id))
                    ];
                }
            }
        }

        wp_enqueue_script('leaflet');
        wp_enqueue_script('leaflet_cluster');
        wp_enqueue_style('leaflet');
        wp_enqueue_style('leaflet_cluster');
        wp_enqueue_style('leaflet_cluster_dflt');
    }

    wp_localize_script('app', 'pharmacy', $localize_script_data);

}

add_action('wp_enqueue_scripts', 'olekach_load_scripts');

function olekach_widgets_init()
{

    register_sidebar(array(
        'name' => 'Apteka: Prawy sidebar',
        'id' => 'sidebar_right_pharmacy',
        'before_widget' => '<div class="widget widget-sidebar-right d-flex flex-column bg-white mb-4 shadow-sm">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="border-bottom p-3 pb-2 m-0 d-flex justify-content-between align-items-center h4 font-weight-light">',
        'after_title' => '</h3>',
    ));

    register_sidebar(array(
        'name' => 'Blog: Prawy sidebar',
        'id' => 'sidebar_right_post',
        'before_widget' => '<div class="widget widget-sidebar-right d-flex flex-column bg-white mb-4 shadow-sm">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="border-bottom p-3 pb-2 m-0 d-flex justify-content-between align-items-center h4 font-weight-light">',
        'after_title' => '</h3>',
    ));

    register_sidebar(array(
        'name' => 'Apteka: Nad treścią',
        'id' => 'sidebar_above_content',
        'before_widget' => '<div class="widget widget-sidebar-right d-flex flex-column bg-white mb-4 shadow-sm">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="border-bottom p-3 pb-2 m-0 d-flex justify-content-between align-items-center h4 font-weight-light">',
        'after_title' => '</h3>',
    ));

    register_sidebar(array(
        'name' => 'Lista Aptek: Pod listą',
        'id' => 'pharmacies_after_list',
        'before_widget' => '<div class="widget widget-sidebar-right d-flex flex-column bg-white mb-4 shadow-sm">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="border-bottom p-3 pb-2 m-0 d-flex justify-content-between align-items-center h4 font-weight-light">',
        'after_title' => '</h3>',
    ));

}

add_action('widgets_init', 'olekach_widgets_init');

/**
 * Add/Change menu list item to add some bootstrap classes
 *
 * @param type $classes
 * @param type $item
 * @param type $args
 * @return type
 */
function olekach_nav_menu_css_class($classes, $item, $args)
{
    if (in_array('current-menu-item', $classes)) {
        $classes[] = 'active font-weight-normal';
    }

    if ($item->menu_item_parent > 0) {
        $classes[] = "border-bottom";
    }

    $classes[] = $args->theme_location == "primary" && $item->menu_item_parent == 0 ? "list-inline-item mx-3" : "list-item";
    return $classes;
}

add_filter('nav_menu_css_class', 'olekach_nav_menu_css_class', 10, 4);

/**
 * Add custom menu header to menu wrapper
 *
 * @param string $args
 * @return string
 */
function olekach_wp_nav_menu_args($args)
{
    $locations = get_nav_menu_locations();
    if ($args['theme_location'] && $locations && isset($locations[$args['theme_location']])) {
        $menu = wp_get_nav_menu_object($locations[$args['theme_location']]);
        $header = get_field('header', $menu);
        if ($header) {
            $args['items_wrap'] = '<h5>' . $header . '</h5><ul id="%1$s" class="%2$s">%3$s</ul>';
        }
    }
    return $args;
}

add_filter('wp_nav_menu_args', 'olekach_wp_nav_menu_args');

function olekach_nav_menu_submenu_css_class($classes, $args, $depth)
{
    $classes[] = "py-0";
    return $classes;
}

add_filter('nav_menu_submenu_css_class', 'olekach_nav_menu_submenu_css_class', 10, 3);
