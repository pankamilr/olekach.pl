<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<li class="nav-item">
    <a class="nav-link h3 p-3 mb-0 border-0 <?php echo ($active_category == $category->term_id) ? "active" : ""; ?>" id="<?php echo $category->slug; ?>-tab" data-toggle="tab" href="#<?php echo $category->slug; ?>" role="tab" aria-controls="<?php echo $category->slug; ?>"
       aria-selected="true"><?php echo $category->name; ?></a>
</li>