<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if( !isset( $category ) || !$category instanceof WP_Term ) {
    return;
}

?>
<div class="tab-pane fade <?php echo ($active_category == $category->term_id) ? "show active" : ""; ?>" id="<?php echo $category->slug; ?>" role="tabpanel" aria-labelledby="<?php echo $category->slug; ?>-tab">
    <div class="banner8 py-3">
        <div class="container">
            <?php if ( $category_description = category_description($category->term_id) ) { ?>
            <div class="row">
                <div class="col">
                    <div class="d-flex p-3 mb-4 border-bottom">
                        <p><?php echo $category_description; ?></p>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="row">
                <?php
                $posts = get_posts( array( 'posts_per_page' => 10, 'cat' => $category->term_id ) );
                $headline_post = reset( $posts );
                $promoted_post = array_slice( $posts, 1, 4 );
                $other_posts = array_slice( $posts, 5 );
                
                if( $headline_post ) { ?>
                    <!-- column  -->
                    <div class="col-lg-5">
                        <div class="p-3 bg-light-yellow">
                            <a href="<?php echo get_the_permalink($headline_post); ?>"><?php echo get_the_post_thumbnail($headline_post, 'heading-thumb', array( 'class' => 'img-fluid border', 'alt' => get_the_title($headline_post) )); ?></a>
                            <h4 class="my-3 h3 font-weight-normal"><a href="<?php echo get_the_permalink($headline_post); ?>"><?php echo get_the_title($headline_post); ?></a></h4>
                            <p class="subtitle mt-3 font-weight-light"><?php echo wp_trim_words( get_the_excerpt($headline_post), 20 ); ?></p>
                            <div class="d-flex justify-content-between align-items-center">
                                <span class="d-flex align-items-center ">
                                    <i class="material-icons mr-3">access_time</i><?php echo get_the_date('d.m.Y', $headline_post); ?></span>
                                <a href="<?php echo get_the_permalink($headline_post); ?>" class="btn bg-dark-green text-white mx-0">Czytaj dalej</a>
                            </div>
                        </div>							
                    </div>
                <?php }
                
                
                if( $promoted_post ) { ?>
                    
                <!-- column  -->
                <div class="col-lg-7">
                    <div class="row">
                        <div class="col-12 col-sm-6 p-3">
                            <div class="row flex-column">
                    <?php foreach( $promoted_post as $j => $promoted_post ) { ?>
                                <div class="col pb-3 <?php echo $j > 0 ? "border-top" : ""; ?>">
                                    <?php if( $j === 0 ) { ?>
                                    <a href="<?php echo get_the_permalink($promoted_post); ?>"><?php echo get_the_post_thumbnail($promoted_post, 'medium', array( 'class' => 'img-fluid border', 'alt' => get_the_title($promoted_post) )); ?></a>
                                    <?php } ?>
                                    <h4 class="mt-3 mb-2 h5 font-weight-normal"><a href="<?php echo get_the_permalink($promoted_post); ?>"><?php echo get_the_title($promoted_post); ?></a></h4>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <small class="d-flex align-items-center ">
                                            <i class="material-icons mr-3">access_time</i><?php echo get_the_date('d.m.Y', $promoted_post); ?></small>
                                        <a href="<?php echo get_the_permalink($promoted_post); ?>" class="text-uppercase badge bg-dark-green font-weight-normal p-2">Czytaj dalej</a>
                                    </div>
                                </div>
                    <?php } ?>
                            </div>
                        </div>
                        <?php
                    if( $other_posts ) { ?>
                        <div class="col-12 col-sm-6 ml-auto p-3">
                        <?php foreach( $other_posts as $other_post ) { ?>
                            <div class="row blog-row mb-3">
                                <div class="col-4">
                                    <a href="<?php echo get_the_permalink($other_post); ?>"><?php echo get_the_post_thumbnail($other_post, 'thumbnail', array( 'class' => 'img-fluid border', 'alt' => get_the_title($other_post) )); ?></a>
                                </div>
                                <div class="col-8">
                                    <h5><a href="<?php echo get_the_permalink($other_post); ?>" class="text-decoration-none h6 font-weight-normal"><?php echo get_the_title($other_post); ?></a></h5>
                                    <small class="text-small d-flex align-items-center ">
                                        <i class="material-icons mr-3">access_time</i><?php echo get_the_date('d.m.Y', $other_post); ?></small>
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                    <?php
                    }
                    ?>
                    </div>
                </div>
                    <?php
                }
                ?>
                
                <!-- column  -->
            </div>
        </div>
    </div>
</div>