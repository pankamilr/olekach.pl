<div class="container py-5">
    <div class="row align-items-center">
        <div class="col-md-4 offset-md-1">
            <h2>Znajdź aptekę w swojej okolicy</h2>
            <?php $city_terms = get_terms( array( 'taxonomy' => 'city', 'orderby' => 'count', 'order' => 'DESC', 'hide_empty' => false, 'number' => 20 ) ); 
            if( $city_terms ) { ?>
            <ul class="list-group mt-5">
            <?php foreach( $city_terms as $term ) { ?>
                <li class="list-group-item d-flex justify-content-between align-items-center"><a href="<?php echo get_term_link($term); ?>">Apteka <?php echo $term->name; ?></a><span class="badge bg-light-red badge-pill"><?php echo $term->count; ?></span></li>
            <?php } ?>
            </ul>
            <?php } ?>
        </div>
        <div class="col-md-5 offset-md-1 d-flex justify-content-center align-items-center">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/2454174.jpg" class="img-fluid" style="max-width:800px;width:100%;" />
        </div>
    </div>
</div>