<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<div class="row blog-row py-3 border-bottom bg-white">
    <div class="col-md-3">
        <a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail(get_the_ID(), 'heading-thumb', array( 'class' => 'img-fluid border', 'alt' => get_the_title() )); ?></a>
    </div>
    <div class="col-md-9">
        <h5><a href="#" class="text-decoration-none h5 font-weight-normal"><?php the_title(); ?></a></h5>
        <p class="subtitle mt-2 font-weight-light d-block text-truncate"><?php echo wp_trim_words( get_the_excerpt(), 20 ); ?></p>
        <small class="text-small d-flex align-items-center "><i class="material-icons mr-3">access_time</i><?php echo get_the_date('d.m.Y'); ?></small>
    </div>
</div>