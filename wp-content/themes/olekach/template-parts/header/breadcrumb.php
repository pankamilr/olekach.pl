<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if ( function_exists('pharmacy_get_breadcrumb') ) { 
	if ( $crumbs = pharmacy_get_breadcrumb() ) { ?>
<ul class="nav breadcrumb bg-transparent" itemscope itemtype="http://schema.org/BreadcrumbList">
	<?php foreach( $crumbs as $i => $crumb ) { 
		$has_children = isset($crumb['children']) && !empty($crumb['children']); 
		?>
	<li class="nav-item d-flex align-items-center breadcrumb-item <?php echo $has_children ? "dropdown" : ""; ?>" itemprop="itemListElement" itemscope
          itemtype="http://schema.org/ListItem">
		<?php if( $has_children ) { ?>
		<a class="nav-link p-0 mr-2 dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"></a>
		<div class="dropdown-menu">
			<?php foreach( $crumb['children'] as $subcrumb ) { ?>
			<a class="dropdown-item" href="<?php echo $subcrumb['url']; ?>"><?php echo $subcrumb['title']; ?></a>
			<?php } ?>
		</div>
		<?php } ?>
		
		<?php if( $crumb['type'] != "current" ) { ?>
                <a itemprop="item" class="nav-link p-0" href="<?php echo $crumb['url']; ?>"><span itemprop="name"><?php echo $crumb['title']; ?></span></a>
		<?php } else { ?>
		<span itemprop="name"><?php echo wordwrap($crumb['title'], 2); ?></span>
		<?php } ?>
                <meta itemprop="position" content="<?php echo $i+1; ?>" />
	</li>
	
	<?php } ?>
</ul>
	<?php }
}