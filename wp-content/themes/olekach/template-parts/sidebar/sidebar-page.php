<?php
/**
 * @author Kamil Ryczek <kamil.ryczek@artefakt.pl>
 * @copyright (c) 2019, Artefakt
 * @package olekach.pl
 */
?>

<?php if ( is_active_sidebar( 'sidebar_right_post' ) ) : ?>
	<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
		<?php dynamic_sidebar( 'sidebar_right_post' ); ?>
	</div><!-- #primary-sidebar -->
<?php endif; ?>
