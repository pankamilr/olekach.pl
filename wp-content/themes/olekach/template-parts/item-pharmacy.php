<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
global $pharmacy_object;
?>
<div class="row no-gutters state py-3 align-items-center <?php echo $pharmacy_object->data->is_open() ? "open" : "closed"; ?> bg-white border-bottom">
    <div class="col">
        <div class="px-3">
            <div class="d-flex align-items-center">
                <div class="distance d-none d-flexy align-items-center mr-2 badge bg-light-red">
                    <i class="material-icons">near_me</i>
                    <span class="ml-1">700 m</span>
                </div>
                <h3 class="h5 m-0 p-0"><a href="<?php echo get_permalink($pharmacy_object->ID); ?>"><?php echo get_the_title($pharmacy_object->ID); ?></a></h3>
            </div>
            <div class="address mt-2 small font-weight-light">
                <?php echo $pharmacy_object->data->address; ?>
            </div>
        </div>
    </div>
    <div class="col-md-4 justify-content-end d-flex ">
        <div class="pharmacy-wrapper d-flex align-items-stretch px-3 px-lg-0 mt-2 mt-lg-0 mr-3">
            <?php
            if ($pharmacy_object->data->is_fulltime()) { ?>
                <span class="state-text d-flex align-items-center bg-light-green small font-weight-light"><i class="material-icons mr-2">schedule</i> Całodobowa</span>
            <?php } else
                if ($pharmacy_object->data->is_open()) { ?>
                    <span class="state-text d-flex align-items-center bg-light-green small font-weight-light"><i class="material-icons mr-2">schedule</i> <?php pharmacy_the_status($pharmacy_object->ID, $pharmacy_object); ?></span>
                    <span class="d-flex align-items-center hours shadow-0 badge text-success px-2">dziś <?php echo $pharmacy_object->data->today()->open; ?> - <?php echo $pharmacy_object->data->today()->close; ?></span>
                    <?php
                } else {
                    if ($next_working_day = $pharmacy_object->data->get_next_working_day()) {
                        ?>
                        <span class="d-flex align-items-center hours shadow-0 badge text-success p-2"><?php echo $next_working_day->day; ?> <?php echo $next_working_day->open; ?> - <?php echo $next_working_day->close; ?></span>
                        <?php
                    }
                }
            ?>

        </div>
    </div>
</div>
