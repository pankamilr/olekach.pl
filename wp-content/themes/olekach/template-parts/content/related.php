<?php
/**
 * Displays the next and previous post navigation in single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

$categories = get_the_category();

if( !empty($categories) ) {
    
    $related_posts = get_posts(array(
        'posts_per_page' => 4,
        'post__not_in' => array( get_the_ID() ),
        'orderby' => 'rand',
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'terms' => array_map(function($e) { return $e->term_id; }, $categories),
            )
        )
    ));        
                
    if( $related_posts ) { ?>
<div class="col">
    <div class="row position-relative pt-4 bg-white mt-5 about-author-wrapper">
        <div class="position-absolute about-label bg-white px-4 pb-1 pt-2 h2">Podobne artykuły</div>
        <?php foreach( $related_posts as $i => $related ) { 
            $orphan = $i+1 == count($related_posts) && ($i+1) % 2 != 0; ?>
        <div class="col-12 col-lg-<?php echo $orphan ? "12" : "6"; ?>">
            <div class="item">
                <div class="row blog-row py-3 bg-white">
                    <?php if(has_post_thumbnail($related) ) { ?>
                    <div class="col-md-<?php echo $orphan ? "2" : "3"; ?>">
                        <a href="<?php the_permalink($related); ?>"><?php echo get_the_post_thumbnail($related, 'heading-thumb', array( 'class' => 'img-fluid border', 'alt' => get_the_title($related) )); ?></a>
                    </div>
                    <?php } ?>
                    <div class="col-md-<?php echo !has_post_thumbnail($related) ? "12" : ( $orphan ? "10" : "9" ); ?>">
                        <h5><a href="<?php the_permalink($related); ?>" class="text-decoration-none h5 font-weight-normal"><?php echo get_the_title($related); ?></a></h5>
                        <p class="subtitle mt-2 font-weight-light d-block text-truncate"><?php echo wp_trim_words( get_the_excerpt($related), 20 ); ?></p>
                        <small class="text-small d-flex align-items-center "><i class="material-icons mr-3">access_time</i><?php echo get_the_date('d.m.Y', $related); ?></small>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
        <?php
    }
}
