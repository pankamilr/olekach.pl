<?php
/**
 * @author Kamil Ryczek <kamil.ryczek@artefakt.pl>
 * @copyright (c) 2019, Artefakt
 * @package olekach.pl
 */

global $post;
?>
<div class="title bg-white px-5 pt-5 pb-4">
    <h1 class="h2 font-weight-light"><?php the_title(); ?></h1>

	<div class="row mt-3">
		<div class="col flex-column d-flex">
			<?php if (function_exists('pharmacy_the_status')) { ?>
	        <div class="time">
	            <small class="d-flex align-items-center "><i class="material-icons mr-3">access_time</i> aktualnie <?php pharmacy_the_status($post->ID, $post); ?></small>
	        </div>
			<?php } ?>

			<div class="address mt-4">
				<address>
					<?php echo $post->data->get_address(true); ?>
				</address>
                            <?php if ($post->data->phone) { ?>
				<p class="mt-4 d-flex align-items-center"><i class="material-icons mr-3">phone_in_talk</i><a href="tel:<?php echo $post->data->phone; ?>"><?php echo $post->data->phone; ?></a></p>
                            <?php } ?>
			</div>

		</div>
		<div class="col-12 col-md-4 flex-column d-flex justify-content-end">
			<?php if (function_exists('pharmacy_the_openings')) { 
				if( $openings = pharmacy_the_openings()) { ?>
                    <h3 class="font-weight-light h6 mb-3">Kiedy otwarte?</h3>
			<ul class="list-group-flush shadow p-0">
				<?php foreach($openings as $day_num => $day) { ?>
                            <li class="list-group-item py-2<?php echo $post->data->today()->day == $day->day ? " font-weight-bold bg-light-green text-dark-green" : ""; ?>"><?php echo $day->day; ?>, <?php echo $day->open; ?> - <?php echo $day->close; ?></li>
				<?php } ?>
			</ul>
                        <?php } 
                        } ?>
			
		</div>
	</div>

</div>
<?php if( $nearest_for_current = get_query_var('current_nearest_pharmacies_distance') ) { ?>
<h2 class="h5 font-weight-light bg-white m-0 p-3">Najbliższe znalezione apteki w promieniu <?php echo $nearest_for_current; ?> km</h2>
<?php } ?>
<div id="mapid" class="map mb-auto" style="min-height:480px;"></div>

