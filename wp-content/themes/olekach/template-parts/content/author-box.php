<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 
global $post;

if (is_singular('post') && $post instanceof WP_Post) {
    $display_name = get_the_author_meta( 'display_name', $post->post_author );
 
    if ( empty( $display_name ) ) {
        $display_name = get_the_author_meta( 'nickname', $post->post_author );
    }
    
    $user_description = get_the_author_meta( 'user_description', $post->post_author );
    $user_website = get_the_author_meta('url', $post->post_author);
}
 
if( !empty($display_name) ) {
?>
<div class="media bg-white pt-5 p-4 border-top position-relative about-author-wrapper">
    <div class="position-absolute about-label bg-white px-4 py-1 h2">Autor</div>
    <?php echo get_avatar( get_the_author_meta('user_email') , 90, '', '', array('class' => 'rounded-circle') ); ?>
    <div class="media-body ml-4">
        <h5 class="mt-0 font-weight-bold"><?php echo $display_name; ?></h5>
        <div class="mt-3 text-muted"><?php echo $user_description; ?></div>
    </div>
</div>
<?php
}
