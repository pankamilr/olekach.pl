<?php
/**
 * @author Kamil Ryczek <kamil.ryczek@artefakt.pl>
 * @copyright (c) 2019, Artefakt
 * @package olekach.pl
 */
?>
<div class="title bg-white px-3 px-md-5 pt-3 pt-md-5 pb-4">
    <h1><?php the_title(); ?></h1>

    <div class="meta d-flex flex-column flex-md-row justify-content-between mt-3">
        <div class="time">
            <small class="d-flex align-items-center "><i class="material-icons mr-3">access_time</i><?php the_date('d.m.Y'); ?></small>
        </div>
        
        <?php
        $post_terms = wp_get_post_terms( get_the_ID(), 'category' );
        if($post_terms) { ?>
        <div class="category mt-3 mt-md-0">
            <ul class="list-inline mb-0">
                <?php foreach($post_terms as $term) { ?>
                <li class="list-inline-item"><a href="<?php echo get_term_link($term); ?>"><?php echo $term->name; ?></a></li>
                <?php } ?>
            </ul>
        </div>
        <?php } ?>
    </div>
</div>
<div class="thumbnail">
    <?php
    if (has_post_thumbnail()) {
        the_post_thumbnail('full', array('class' => 'img-fluid'));
    }
    ?>
</div>
<div class="body bg-white text-lg p-3 pb-5 p-md-5" itemprop="articleBody">
    <?php the_content(); ?>
</div>

<?php get_template_part('template-parts/content/author-box'); ?>

<?php
/**
 *  Output comments wrapper if it's a post, or if comments are open,
 * or if there's a comment number – and check for password.
 * */
if ( ( comments_open() || get_comments_number() ) && !post_password_required() ) {
    ?>
<div class="comments-wrapper section-inner bg-white p-4">
    <?php comments_template(); ?>
</div><!-- .comments-wrapper -->
    <?php
} ?>

<div class="related-posts">
    <?php get_template_part('template-parts/content/navigation' ); ?>
    <?php get_template_part('template-parts/content/related' ); ?>
</div>