<?php

/**
 * @author Kamil Ryczek <kamil@piszekod.pl>
 * @copyright (c) 2019, piszekod.pl
 * @package olekach.pl
 */
?>

		<div class="container">
			<footer class="pt-4 my-md-5 pt-md-5 border-top">
				<div class="row">
					<div class="col-12 col-md text-center">
						<img class="mb-2 img-fluid" src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="" style="max-width: 120px;">
						<small class="d-block mb-3 text-muted">&copy; <?php echo date("Y"); ?></small>
					</div>
					<div class="col-6 col-md">
                                            <?php wp_nav_menu(array(
                                                'theme_location' => 'footer_1',
                                                'menu_class' => 'list-unstyled text-small m-0 p-4 p-md-0',
                                            )); ?>
					</div>
					<div class="col-6 col-md">
						<?php wp_nav_menu(array(
                                                    'theme_location' => 'footer_2',
                                                    'container' => 'ul',
                                                    'menu_class' => 'list-unstyled text-small m-0 p-4 p-md-0',
                                                )); ?>
					</div>
					<div class="col-6 col-md">
						<?php wp_nav_menu(array(
                                                    'theme_location' => 'footer_3',
                                                    'container' => 'ul',
                                                    'menu_class' => 'list-unstyled text-small m-0 p-4 p-md-0',
                                                )); ?>
					</div>
				</div>
			</footer>
		</div>
		<?php wp_footer(); ?>
                
	</body>
</html>
