<?php
/**
 * @author Kamil Ryczek <kamil@piszekod.pl>
 * @copyright (c) 2019, O Lekach
 * @package olekach.pl
 */
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!--<link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">-->
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" />
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>

		<div class="d-flex flex-row align-items-center justify-content-end p-3 px-md-5 navbar-wrapper" data-toggle="affix">
			<div class="h2 my-0 mr-auto">
                            <a href="<?php echo home_url(); ?>" class="text-decoration-none">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" class="img-fluid logo mr-0 mr-md-3" />
                                <span class="d-none d-md-inline-block"><?php echo get_bloginfo( 'blogname' ); ?></span>
                            </a>
                        </div>
			<nav class="my-2 my-md-0 mr-md-3 d-flex">
                            <ul class="list-inline m-0 p-0 d-flex align-items-center">
                            <?php if( !is_front_page() ) { ?>
                                <li class="list-inline-item">
                                    <a data-toggle="collapse" href="#" data-target=".search-section" role="button" aria-expanded="false" aria-controls="search-section" class="d-flex align-self-baseline mr-3">
                                        <i class="material-icons">search</i>
                            </a></li>
                            <?php } ?>
                                </ul>
                            
                            <?php 
                            wp_nav_menu( array(
                                    'theme_location'  => 'primary',
                                    'depth'           => 2, // 1 = no dropdowns, 2 = with dropdowns.
                                    'container'       => 'div',
                                    'container_class' => 'collapse navbar-collapse show justify-content-center',
                                    'container_id'    => 'bs-example-navbar-collapse-1',
                                    'menu_class'      => 'list-inline m-0 navbar-nav mr-auto flex-row',
                                    'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
                                    'walker'          => new WP_Bootstrap_Navwalker(),
                                ) );
                            ?>
			</nav>
			<a class="btn bg-dark-green text-white box-shadow localizeme d-flex align-items-center" href="#">
                            <i class="material-icons mr-4 mr-md-0">gps_fixed</i>
                            <span class="d-none d-md-inline-block ml-3">Apteki w mojej okolicy</span>
                        </a>                    
                        
		</div>
	    
            <?php            get_search_form(); ?>
                
            <?php if( ! is_front_page() ) { ?>
		<div class="container">
			<div class="row">
				<div class="col">
                                    <?php get_template_part('template-parts/header/breadcrumb'); ?>
                                </div>
			</div>
		</div>
            <?php } ?>

            <?php  if ( is_active_sidebar( 'sidebar_above_content' ) ) : ?>
	<div id="pharma-main-map" class="primary-sidebar widget-area" role="complementary">
		<?php dynamic_sidebar( 'sidebar_above_content' ); ?>
	</div><!-- #primary-sidebar -->
            <?php endif;  ?>