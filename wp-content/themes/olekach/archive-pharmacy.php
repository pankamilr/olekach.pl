<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

get_header();

$cities = get_terms( array('taxonomy' => 'city' ) );

$alphabeth = array();

foreach($cities as $city) {
    $first_letter = mb_substr($city->name, 0, 1);
    if(!isset($alphabeth[$first_letter])) {
        $alphabeth[$first_letter] = array();
    }
    
    $alphabeth[$first_letter][] = $city;
}
?>

<div class="container">
    <div class="row mb-5 d-none d-md-flex">
        <div class="col">
            <div class="btn-toolbar justify-content-center" role="toolbar" aria-label="Toolbar with button groups">
                <div class="btn-group mr-2 alphabeth-list" role="group" aria-label="First group">
                    <?php foreach( array_keys($alphabeth) as $letter ) { ?>
                    <button type="button" class="btn btn-white scroll-to" data-scroll-to="<?php echo $letter; ?>"><?php echo $letter; ?></button>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <?php foreach( $alphabeth as $letter => $items ) { ?>
            <fieldset class="border-top pl-0 pl-md-5" id="scroll-to-<?php echo $letter; ?>">
                <legend class="w-auto font-weight-bold px-5"><?php echo $letter; ?></legend>
                <ul class="list-unstyled row">
                    <?php foreach( $items as $city ) { ?>
                    <li class="col-12 col-md-3 list-group-item py-2 bg-transparent border-0 d-flex align-items-center">
                        <a href="<?php echo get_term_link($city); ?>"><?php echo $city->name; ?></a><span class="ml-3 badge bg-light-red badge-pill"><?php echo $city->count; ?></span></li>
                    <?php } ?>
                </ul>
            </fieldset>
            <?php } ?>
        </div>
    </div>
</div>

<?php
get_footer();
