<?php
/**
 * @author Kamil Ryczek <kamil.ryczek@artefakt.pl>
 * @copyright (c) 2019, Artefakt
 * @package olekach.pl
 */
get_header();
?>

<div class="container">
    <div class="row flex-row-reverse ">
        <div class="col-12 col-lg-4 order-1 mt-4 mt-lg-0 order-lg-0">
<?php get_sidebar('post'); ?>
        </div>

        <div class="col-12 col-lg-8 d-flex">
            <div class="pr-0 pr-lg-5 w-100 d-flex flex-column align-items-stretch justify-content-between">
            <?php
            if (have_posts()) {
                while (have_posts()) {
                    the_post();
                    get_template_part('template-parts/content/content', get_post_type());
                }
            }
            ?>
            </div>
        </div>
    </div>	
</div>

<?php
get_footer();
