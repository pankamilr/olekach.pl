/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function($) {
    function onLocationTrigger(e) {
        e.preventDefault();
        toastr.info('Szukam Twojej lokalizacji...');

        var localizeMapDiv = document.createElement('div');
        localizeMapDiv.setAttribute("id", "localizeMap");
        localizeMapDiv.style.width = "0";
        localizeMapDiv.style.height = "0";

        var map = L.map(localizeMapDiv).setView([0,0], 16);
        map.on('locationfound', onLocationFound);
        map.on('locationerror', onLocationError);
        map.locate({enableHighAccuracy: true});
    }

    function onLocationFound(e) {
        Cookies.set("currentUserLatLng", e.latlng.lat+";"+e.latlng.lng);
        var ajaxCall = jQuery.ajax({
            dataType: 'json',
            url: 'https://nominatim.openstreetmap.org/search?q='+e.latlng.lat+','+e.latlng.lng+'&format=json&polygon=0&addressdetails=1'
        });
        
        ajaxCall.done(function(data) {
            if(data.length) {
                var location = data.shift();
                Cookies.set("currentUserCity", location.address.city);
                jQuery.ajax({
                    dataType: 'json',
                    data: { taxonomy: 'city', lookup: location.address.city, action: 'search_terms' },
                    url: pharmacy.ajaxurl
                })
                .done(function(termdata) {
                    if(termdata && typeof termdata.url !== 'undefined') {
                        toastr.success('Lokalizacja odnaleziona. Przekierowuje...');
                        Cookies.set("currentUserCityTerm", termdata.term_id);
                        window.location.replace(termdata.url);
                    }
                });
            }
        });
    }

    function onLocationError(e) {
        toastr.error("Nie odnaleziono lokalizacji. Spróbuj ponownie.");
    }

    function initalizeSinglePharmacyMap(pharmacyData) {
        var grayscale   = L.tileLayer(pharmacy.map_url, {id: 'mapbox.light'}),
            streets  = L.tileLayer(pharmacy.map_url, {id: 'mapbox.streets'}),
            pharmacyMap = L.map('mapid', {
                layers: [grayscale],
                center: [pharmacyData.data.latitude, pharmacyData.data.longitude],
                zoom: 16
            }),
            baseLayers = {
                "Monochromatyczna": grayscale,
                "Kolorowa": streets
            },
            siblingsPharmaciesIcon = L.icon({
                iconUrl: pharmacy.single_pharmacy.map.icon_path + '/assets/img/marker-icon_blue.png',
                shadowUrl: pharmacy.single_pharmacy.map.icon_path + '/assets/img/marker-shadow.png',
                iconSize:[25,41],
                iconAnchor:[12,41],
                popupAnchor:[1,-34],
                tooltipAnchor:[16,-28],
                shadowSize:[41,41]
            });

            L.control.layers(baseLayers).addTo(pharmacyMap);
            L.marker([pharmacyData.data.latitude, pharmacyData.data.longitude]).addTo(pharmacyMap).bindPopup(pharmacyData.post_title).openPopup();

            $.each(pharmacy.single_pharmacy.map.siblings, function(i, e) {
                L.marker([e.lat, e.long], {icon: siblingsPharmaciesIcon}).addTo(pharmacyMap).bindPopup(e.title + e.link);
            });

    }
    
    $(document).ready(function($) { 
        $(".localizeme").on("click", onLocationTrigger);
        
        $('[data-cookie="set"]').on("click", function() {
            Cookie.set('policyAccept', true, { expires: 180 });
        });
        
        $('[data-cookie="deny"]').on("click", function() {
            Cookie.set('policyAccept', true, {expires: 1});
        });

        $(".cities-roll").on("click", function(){
            $(this).prev().toggleClass("roll-up");
            $(this).toggleClass("d-block");
        }); 
        
        $('[href=".search-section"]').on("click", function() {
            $('html,body').animate({ scrollTop: 0 }, 500);
        });

        if ($('body').hasClass('single-pharmacy') && undefined !== pharmacy.current_term.post_type) {
            initalizeSinglePharmacyMap(pharmacy.current_term);
        }

        $('.scroll-to').on("click", function(el) {
           var currentLetter = $(this).data("scroll-to");
            $('html, body').animate({
                scrollTop: $("#scroll-to-" + currentLetter).offset().top - 100
            }, 1000);
        });

        var test = window.setInterval(function(){
       }, 2000);

        setTimeout(function() {
           clearInterval(test);
        }, 10000);
    });
    
    $(window).on("scroll", function(e) {
        var scroll = $(window).scrollTop();
        if ( scroll > 50 && !$("body").hasClass("fixed") ) {
            $("body").addClass("fixed");
        } 
        
        if ( scroll < 50 && $("body").hasClass("fixed") ) {
            $("body").removeClass("fixed");
        }
    });
    
})(jQuery);
