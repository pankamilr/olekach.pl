<div class="search-section px-3 py-3 pt-md-3 pb-md-4 mx-auto text-center collapse <?php echo is_front_page() ? "show" : ""; ?>">
    <form class="form-inline md-form form-sm active-cyan-2 mt-2" method="GET" action="<?php echo home_url(); ?>">
        <div class="form-group flex-grow-1">
            <input class="form-control form-control-sm mr-3 text-center search-large w-100" type="text" name="s" placeholder="czego szukasz?"
                   aria-label="Search">
        </div>
        <button type="submit" class="btn btn-link btn-search-submit"><i class="material-icons">search</i></button>
    </form>

</div>