<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

get_header();

if (have_posts()) {
    ?>
    <?php
    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */


    get_header();
    ?>
    <div class="container">
        <h1 class="text-center">Szukane slowo</h1>
        <h2 class="h5 font-weight-light pb-5 mb-5 text-center">Drugi headline</h2>
        <div class="row">
            <div class="col-12 col-lg-8">
                <div class="pr-5">

                    <?php
                    while (have_posts()) {
                        the_post();
                        get_template_part('template-parts/item', get_post_type());
                    }

                    the_posts_pagination(
                            array(
                                'type' => 'list',
                                'screen_reader_text' => " ",
                                'mid_size' => 2,
                                'prev_text' => "Poprzednie",
                                'next_text' => "Następne"
                            )
                    );
                    ?>
                </div>
            </div>

            <div class="col-12 col-lg-4">
                <?php get_sidebar('pharmacy'); ?>
            </div>
        </div>

    </div>

    <?php
}

get_footer();
