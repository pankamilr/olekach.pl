<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

get_header();
?>
<div class="container">
    <div class="row">
        <div class="col">
            <ul class="nav nav-tabs border-0" id="myTab" role="tablist">
                <li class="nav-item">
                    <h1 class="nav-link active h3 p-3 mb-0 border-0" id="test-tab" data-toggle="tab" href="#test" role="tab" aria-controls="test"
                        aria-selected="true"><?php single_term_title(); ?></h1>
                </li>
            </ul>

            <div class="tab-content bg-white box-shadow" id="myTabContent">
                <div class="tab-pane fade show active" id="test" role="tabpanel" aria-labelledby="test-tab">
                    <div class="banner8 py-3">
                        <div class="container">
                            <?php if ( $category_description = category_description() ) { ?>
                            <div class="row">
                                <div class="col">
                                    <div class="d-flex p-3 mb-4 border-bottom">
                                        <p><?php echo $category_description; ?></p>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                            
                            <?php if( have_posts() ) {
                                global $wp_query;
                                $posts = $wp_query->posts;
                                $headlines = [];

                                if(get_query_var('paged') == 0) {
                                    $headlines = array_slice($posts, 0, 3, true);
                                    $posts = array_slice($posts, 3);
                                }
                                ?>
                            <?php if($headlines) { ?>
                            <!-- HEADLINES -->
                            <div class="row">
                                <?php foreach( $headlines as $headline ) { ?>
                                <!-- column  -->
                                <div class="col-lg-4 d-flex">
                                    <div class="p-3 bg-light-yellow d-flex flex-column">
                                        <a href="<?php echo get_the_permalink($headline); ?>"><?php echo get_the_post_thumbnail($headline, 'heading-thumb', array( 'class' => 'img-fluid border', 'alt' => get_the_title($headline) )); ?></a>
                                        <h4 class="my-3 h3 font-weight-normal"><?php echo get_the_title($headline); ?></h4>
                                        <p class="subtitle mt-3 font-weight-light"><?php echo wp_trim_words( get_the_excerpt($headline), 20 ); ?></p>
                                        <div class="d-flex justify-content-between align-items-center mt-auto">
                                            <span class="d-flex align-items-center ">
                                                <i class="material-icons mr-3">access_time</i><?php echo get_the_date('d.m.Y', $headline); ?></span>
                                            <a href="<?php echo get_permalink($headline); ?>" class="btn bg-dark-green text-white mx-0">Czytaj dalej</a>
                                        </div>

                                    </div>							
                                </div>
                                <!-- column  -->
                                <?php } ?>
                            </div>
                            <!-- /HEADLINES -->
                                <?php } ?>
                            
                            <div class="row mt-4">
                                <?php if($posts) { ?>
                                <!-- PROMOTED -->
                                <div class="col-12 col-lg-8 p-3">
                                    <div class="inner px-3">
                                        <?php foreach( $posts as $promote ) { ?>
                                        <div class="row blog-row pb-3 border-bottom mb-3">
                                            <div class="col-md-3">
                                                <a href="<?php echo get_the_permalink($promote); ?>"><?php echo get_the_post_thumbnail($promote, 'heading-thumb', array( 'class' => 'img-fluid border', 'alt' => get_the_title($headline) )); ?></a>
                                            </div>
                                            <div class="col-md-9">
                                                <h5><a href="<?php echo get_the_permalink($promote); ?>" class="text-decoration-none h5 font-weight-normal"><?php echo get_the_title($promote); ?></a></h5>
                                                <p class="subtitle mt-2 font-weight-light d-block text-truncate"><?php echo wp_trim_words( get_the_excerpt($promote), 20 ); ?></p>
                                                <small class="text-small d-flex align-items-center "><i class="material-icons mr-3">access_time</i><?php echo get_the_date('d.m.Y', $promote); ?></small>
                                            </div>
                                        </div>
                                        <?php } ?>

                                        <?php
                                        the_posts_pagination(
                                            array(
                                                'type' => 'list',
                                                'screen_reader_text' => " ",
                                                'mid_size' => 2,
                                                'prev_text' => "Poprzednie",
                                                'next_text' => "Następne"
                                            )
                                        );
                                        ?>
                                    </div>
                                </div>
                                <!-- /PROMOTED -->
                                <?php } ?>
                                
                                <div class="col-12 col-lg-4">
                                    <div class="row">
                                        
                                        <!-- MOST COMMENTED -->
                                        <div class="d-none col-12 col-md-6 p-3">
                                            <div class="d-flex flex-column bg-white mb-4">
                                                <h3 class="border-bottom h5 pb-3 mb-0 d-flex justify-content-between align-items-center">Najczęściej komentowane:</h3>
                                            </div>
                                        </div>
                                        <!-- /MOST COMMENTED -->
                                        
                                        <?php $post_other_categories = get_posts( array('posts_per_page' => 10, "category__not_in" => array(get_queried_object_id())) );
                                        if($post_other_categories) { ?>
                                        <!-- OTHERS -->
                                        <div class="col-12 col-md-12 ml-auto p-3">

                                            <div class="d-flex flex-column bg-white mb-4">
                                                <h3 class="border-bottom h5 pb-3 mb-0 d-flex justify-content-between align-items-center">Może zainteresuje Cię również:</h3>
                                            </div>
                                            <?php foreach($post_other_categories as $poc) { 
                                                $terms = get_the_terms($poc, 'category'); 
                                                $category_badge = !is_wp_error($terms) ? sprintf('<a href="%s" class="badge badge-info py-1 font-weight-light mr-1">%s</a>', get_term_link( reset($terms) ), reset($terms)->name) : ""; ?>
                                            <div class="row blog-row mb-2">
                                                <div class="col border-bottom pb-2">
                                                    <h5><?php echo $category_badge; ?> <a href="<?php echo get_permalink($poc); ?>" class="text-decoration-none h6 font-weight-normal"><?php echo get_the_title($poc); ?></a></h5>
                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <!-- /OTHERS -->
                                        <?php } ?>
                                    </div>
                                </div>

                                <!-- column  -->
                            </div>
                            <?php } else { ?>
                            <p class="h1">Brak wpisów w tej kategorii</p>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<?php
get_footer();
