<?php
/**
 * @author Kamil Ryczek <kamil@piszekod.pl>
 * @copyright (c) 2019, O Lekach
 * @package olekach.pl
 */
get_header();
?>


<div class="container">
    <div class="row">
        <div class="col">
            <?php 
            $categories = get_categories(array('hide_empty' => true)); 
            
            if( $categories ) { 
                $tabs_list = $tabs_content = ""; 
                $active_category = false;
                foreach( $categories as $i => $category ) {
                    echo "<!-- ii = " . $i . " -->";
                    if ( false === $active_category ) {
                        set_query_var( 'active_category', $category->term_id );
                        $active_category = $category;
                    }
                    set_query_var( 'category', $category );
                    
                    ob_start();
                    get_template_part('template-parts/frontpage/category-tab', 'list');
                    $tabs_list .= ob_get_clean();
                    
                    ob_start();
                    get_template_part('template-parts/frontpage/category-tab', 'content');
                    $tabs_content .= ob_get_clean();
                        
                } 
                ?>
            <ul class="nav nav-tabs border-bottom-0" id="myTab" role="tablist">
                <?php echo $tabs_list; ?>
            </ul>
           
            <div class="tab-content bg-white box-shadow" id="myTabContent">
                <?php echo $tabs_content; ?>
            </div>
                <?php 
            }
            ?>
        </div>
    </div>

</div>

<?php get_template_part('template-parts/block', 'find-pharmacy'); ?>

<div class="container">
<?php
if (have_posts()) :
    while (have_posts()) :
        the_post();

    endwhile;
endif;
?>
</div>
    <?php
    get_footer();
    