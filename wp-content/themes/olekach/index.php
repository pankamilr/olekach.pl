<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

get_header();
?>
<div class="container">
    <div class="row">
        <div class="col-12 col-lg-8">
<?php
if (have_posts() ) { ?>
            <div class="bg-white px-3">
    <?php 
    while(have_posts()) {
        the_post();
        ?>
        <div class="row blog-row pb-3 pt-3 border-bottom mb-0">
            <div class="col-md-2">
                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('heading-thumb', array( 'class' => 'img-fluid border', 'alt' => get_the_title() )); ?></a>
            </div>
            <div class="col-md-10">
                <h5><a href="<?php the_permalink(); ?>" class="text-decoration-none h5 font-weight-normal"><?php echo get_the_title(); ?></a></h5>
                <p class="subtitle mt-2 mb-2 font-weight-light d-block"><?php echo wp_trim_words( get_the_excerpt(), 30 ); ?></p>
                <div class="meta d-flex flex-column flex-md-row align-items-md-center">
                    <small class="text-small d-flex align-items-center "><i class="material-icons mr-3">access_time</i><?php echo get_the_date('d.m.Y'); ?></small>
                    
                    <div class="term-badges d-flex justify-content-end ml-md-3">
                    <?php foreach( get_the_category() as $category ) { ?>
                        <span class="bg-light-silver text-dark-silver px-2 py-1 small mx-1"><?php echo $category->name; ?></span>
                    <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
            </div>
    <?php
    the_posts_pagination(
            array(
                'type' => 'list',
                'screen_reader_text' => " ",
                'mid_size' => 2,
                'prev_text' => "Poprzednie",
                'next_text' => "Następne"
            )
    );
                            
}
?>
        </div>
        <div class="col-12 col-lg-4">
            <?php get_sidebar('pharmacy'); ?>
        </div>
<?php
get_footer();
