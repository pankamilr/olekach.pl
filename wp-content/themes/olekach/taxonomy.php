<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


get_header();
?>
<div class="container">
    <h1 class="text-center">Apteka <?php single_term_title(); ?></h1>
    <h2 class="h5 font-weight-light pb-5 m-0 text-center">Sprawdź, która apteka w mieście <?php single_term_title(); ?> jest otwarta 24h</h2>
    <div class="row">

        <div class="col-12 col-lg-8">
            <h2 class="h5 font-weight-light mb-4">W mieście <?php single_term_title(); ?> jest <?php echo get_queried_object()->count; ?> aktualnie dostępnych aptek</h2>
            <div class="row mb-5 mb-lg-0">
                <div class="col">
                    <div class="pr-lg-5">
                        <?php if (have_posts()) {                            
                            $fulltime_pharmacies = get_fulltime_pharmacies(); ?>

                            <ul class="nav nav-tabs border-bottom-0" id="pharmaciesTab" role="tablist">
                                <li class="nav-item d-flex align-items-end">
                                    <h2 class="m-0 p-3 nav-link active c-pointer h4 font-weight-light" id="regular-openings-tab" data-toggle="tab" href="#regular-openings" role="tab" aria-controls="regular-openings"
                                        aria-selected="true">Apteki <?php single_term_title(); ?></h2>
                                </li>
                                <?php if( !empty($fulltime_pharmacies) ) { ?>
                                <li class="nav-item d-flex align-items-end">
                                    <h2 class="m-0 p-3 nav-link font-weight-light c-pointer h4 font-weight-light" id="fulltime-openings-tabs" data-toggle="tab" href="#fulltime-openings" role="tab" aria-controls="fulltime-openings"
                                        aria-selected="false">Apteki całodobowe <?php single_term_title(); ?></h2>
                                </li>
                                <?php } ?>
                            </ul>
                        <div class="tab-content bg-white" id="pharmaciesTabContent">
                    
                        <div class="tab-pane fade active show" id="regular-openings" role="tabpanel" aria-labelledby="regular-openings-tab">
                            <div class="pharmacies-list bg-white">

                                <?php
                                $pharmacies = get_pharmacies_in_city();
                                foreach ($pharmacies as $pharmacy) {
                                    global $pharmacy_object;
                                    $pharmacy_object = $pharmacy;

                                    get_template_part('template-parts/item', 'pharmacy');
                                }
                                ?>

                            </div>

                            <?php
                            the_posts_pagination(
                                    array(
                                        'type' => 'list',
                                        'screen_reader_text' => " ",
                                        'mid_size' => 2,
                                        'prev_text' => "Poprzednie",
                                        'next_text' => "Następne"
                                    )
                            );
                            ?>
                        </div>
                        <?php if( !empty($fulltime_pharmacies) ) { ?>
                        <div class="tab-pane fade" id="fulltime-openings" role="tabpanel" aria-labelledby="fulltime-openings-tab">
                            <div class="pharmacies-list bg-white">
                            <?php 
                            global $wp_query; 
                                $f_pharmacies = new WP_Query(array(
                                    'post_type' => 'pharmacy',
                                    'posts_per_page' => -1,
                                    'post__in' => $fulltime_pharmacies
                                ));

                                if($f_pharmacies->have_posts()) { 
                                    $main_query = $wp_query;
                                    $wp_query = $f_pharmacies;
                                    while( have_posts() ) {
                                        the_post();
                                        global $pharmacy_object, $post;
                                        $pharmacy_object = $post;

                                        get_template_part('template-parts/item', 'pharmacy');
                                    }

                                    $wp_query = $main_query;
                                    wp_reset_postdata();
                                }
                            ?>
                            </div>
                        </div>
                        <?php } ?>
                        </div>
<?php } ?>              
                    </div>
                    
                </div>                
            </div>
        </div>

        <div class="col-12 col-lg-4">
                <?php if ($term_description = term_description()) { ?>
                <div class="d-flex flex-column p-3 mb-4">
                    <p class="h5 d-flex align-items-center mb-4"><i class="material-icons mr-3">chat_bubble_outline</i> Informacje o aptekach w <?php single_term_title(); ?></p>
                <?php echo $term_description; ?>
                </div>
<?php } ?>        

<?php get_sidebar('pharmacy'); ?>
        </div>
    </div>
    
    <?php get_template_part('template-parts/block', 'find-pharmacy'); ?>
    
                    
    <?php if ( is_active_sidebar( 'pharmacies_after_list' ) ) : ?>
    <div id="pharmacies-after-list-sidebar" class="pharmacies-after-list-sidebar widget-area" role="complementary">
        <?php dynamic_sidebar( 'pharmacies_after_list' ); ?>
    </div><!-- #primary-sidebar -->
    <?php endif; ?>

</div>

<?php 
    get_footer();
    
