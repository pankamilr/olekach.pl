��          �       �       �   �   �   X   �     3  n   E     �  =   �  (     i   /  �   �     �     �  
   �    �  M  �  `   	     b	  y   	  !   �	  E   
  9   a
  }   �
  �             %     7   15 new review box templates, 15 new Schema types, commment reviews, user can rate each feature, review popups, review notification bars, custom width, 9 new custom widgets, Google reviews, Facebook reviews, Yelp reviews and much more... Here you can import your existing user ratings from WP Review 1.x and WP Review Pro 1.x. Hide User Reviews If you are changing User Rating Type & post already have user ratings, please edit or remove existing ratings. Record User Ratings Remember to update the passwords and roles of imported users. Restrict rating to registered users only Thank you for updating WP Review Pro. Your existing user ratings will show up after importing them in %s. To make it easier for you to edit and save the imported content, you may want to reassign the author of the imported item to an existing user of this site. For example, you may want to import all the entries as <code>admin</code>s entries. User Review User Reviews vote votes Project-Id-Version: WP Review
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language-Team: Polski
Last-Translator: artefakt <patryk.wichrowski@artefakt.pl>
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;_ex:1,2c;_n:1,2;_n_noop:1,2;_nx:1,2,4c;_nx_noop:1,2,3c;_x:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-SourceCharset: UTF-8
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10 >= 2 && n%10<=4 &&(n%100<10||n%100 >= 20)? 1 : 2);
POT-Creation-Date: 2018-11-21 12:17+0000
PO-Revision-Date: 2018-12-11 07:37+0000
X-Generator: Loco https://localise.biz/
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
Report-Msgid-Bugs-To: 
Language: pl_PL
X-Loco-Version: 2.2.0; wp-4.9.8 15 nowych szablonów pól opinii, 15 nowych typów schematów, przeglądy commment, użytkownik może ocenić każdą funkcję, przeglądać wyskakujące okienka, paski powiadomień przeglądu, niestandardową szerokość, 9 nowych niestandardowych widżetów, opinie Google, recenzje na Facebooku, recenzje Yelp i wiele więcej ...
 Tutaj możesz importować istniejące oceny użytkowników z WP Review 1.x i WP Review Pro 1.x.
 Ukryj opinie użytkowników
 Jeśli zmieniasz typ oceny użytkownika Wpisz i opublikuj już oceny użytkowników, edytuj lub usuń istniejące oceny.
 Zarejestruj oceny użytkowników
 Pamiętaj o aktualizacji haseł i ról importowanych użytkowników.
 Ogranicz ocenę tylko do zarejestrowanych użytkowników
 Dziękujemy za aktualizację WP Review Pro. Twoje istniejące oceny użytkowników pojawią się po zaimportowaniu ich w% s.
 Aby ułatwić edytowanie i zapisywanie zaimportowanej treści, możesz ponownie przypisać autora zaimportowanego elementu do istniejącego użytkownika tej witryny. Na przykład możesz zaimportować wszystkie wpisy jako wpisy <code> admin </ code>.
 Ocena artykułu Ocena artykułów głos głosów głosy 